package com.xs.heart.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.xs.heart.service.HeartContactApplyService;
import com.xs.heart.entity.HeartContactApply;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 好友申请表 前端控制器
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
@Controller
@RequestMapping("/heart-contact-apply")
public class HeartContactApplyAction {


    @Autowired
    private HeartContactApplyService heartContactApplyService;

    @GetMapping(value = "/")
    public ResponseEntity<Page<HeartContactApply>> list(@RequestParam(required = false) Integer current, @RequestParam(required = false) Integer pageSize) {
        if (current == null) {
            current = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        Page<HeartContactApply> aPage = heartContactApplyService.page(new Page<>(current, pageSize));
        return new ResponseEntity<>(aPage, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<HeartContactApply> getById(@PathVariable("id") String id) {
        return new ResponseEntity<>(heartContactApplyService.getById(id), HttpStatus.OK);
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Object> create(@RequestBody HeartContactApply params) {
        heartContactApplyService.save(params);
        return new ResponseEntity<>("created successfully", HttpStatus.OK);
    }

    @PostMapping(value = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        heartContactApplyService.removeById(id);
        return new ResponseEntity<>("deleted successfully", HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<Object> update(@RequestBody HeartContactApply params) {
        heartContactApplyService.updateById(params);
        return new ResponseEntity<>("updated successfully", HttpStatus.OK);
    }
}
