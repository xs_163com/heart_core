package com.xs.heart.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.xs.heart.service.HeartMessageService;
import com.xs.heart.entity.HeartMessage;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 用户聊天详细内容表 前端控制器
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
@Controller
@RequestMapping("/heart-message")
public class HeartMessageAction {


    @Autowired
    private HeartMessageService heartMessageService;

    @GetMapping(value = "/")
    public ResponseEntity<Page<HeartMessage>> list(@RequestParam(required = false) Integer current, @RequestParam(required = false) Integer pageSize) {
        if (current == null) {
            current = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        Page<HeartMessage> aPage = heartMessageService.page(new Page<>(current, pageSize));
        return new ResponseEntity<>(aPage, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<HeartMessage> getById(@PathVariable("id") String id) {
        return new ResponseEntity<>(heartMessageService.getById(id), HttpStatus.OK);
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Object> create(@RequestBody HeartMessage params) {
        heartMessageService.save(params);
        return new ResponseEntity<>("created successfully", HttpStatus.OK);
    }

    @PostMapping(value = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        heartMessageService.removeById(id);
        return new ResponseEntity<>("deleted successfully", HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<Object> update(@RequestBody HeartMessage params) {
        heartMessageService.updateById(params);
        return new ResponseEntity<>("updated successfully", HttpStatus.OK);
    }
}
