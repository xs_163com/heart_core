package com.xs.heart.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.xs.heart.service.HeartUserService;
import com.xs.heart.entity.HeartUser;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
@Controller
@RequestMapping("/heart-user")
public class HeartUserAction {


    @Autowired
    private HeartUserService heartUserService;

    @GetMapping(value = "/")
    public ResponseEntity<Page<HeartUser>> list(@RequestParam(required = false) Integer current, @RequestParam(required = false) Integer pageSize) {
        if (current == null) {
            current = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        Page<HeartUser> aPage = heartUserService.page(new Page<>(current, pageSize));
        return new ResponseEntity<>(aPage, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<HeartUser> getById(@PathVariable("id") String id) {
        return new ResponseEntity<>(heartUserService.getById(id), HttpStatus.OK);
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Object> create(@RequestBody HeartUser params) {
        heartUserService.save(params);
        return new ResponseEntity<>("created successfully", HttpStatus.OK);
    }

    @PostMapping(value = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        heartUserService.removeById(id);
        return new ResponseEntity<>("deleted successfully", HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<Object> update(@RequestBody HeartUser params) {
        heartUserService.updateById(params);
        return new ResponseEntity<>("updated successfully", HttpStatus.OK);
    }
}
