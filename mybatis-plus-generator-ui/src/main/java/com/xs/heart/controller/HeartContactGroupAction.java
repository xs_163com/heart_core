package com.xs.heart.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.xs.heart.service.HeartContactGroupService;
import com.xs.heart.entity.HeartContactGroup;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 好友分组表 前端控制器
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
@Controller
@RequestMapping("/heart-contact-group")
public class HeartContactGroupAction {


    @Autowired
    private HeartContactGroupService heartContactGroupService;

    @GetMapping(value = "/")
    public ResponseEntity<Page<HeartContactGroup>> list(@RequestParam(required = false) Integer current, @RequestParam(required = false) Integer pageSize) {
        if (current == null) {
            current = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        Page<HeartContactGroup> aPage = heartContactGroupService.page(new Page<>(current, pageSize));
        return new ResponseEntity<>(aPage, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<HeartContactGroup> getById(@PathVariable("id") String id) {
        return new ResponseEntity<>(heartContactGroupService.getById(id), HttpStatus.OK);
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Object> create(@RequestBody HeartContactGroup params) {
        heartContactGroupService.save(params);
        return new ResponseEntity<>("created successfully", HttpStatus.OK);
    }

    @PostMapping(value = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        heartContactGroupService.removeById(id);
        return new ResponseEntity<>("deleted successfully", HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<Object> update(@RequestBody HeartContactGroup params) {
        heartContactGroupService.updateById(params);
        return new ResponseEntity<>("updated successfully", HttpStatus.OK);
    }
}
