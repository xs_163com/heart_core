// package com.xs.heart.controller;
//
// import org.springframework.web.bind.annotation.*;
// import org.springframework.stereotype.Controller;
// import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
// import org.springframework.http.HttpStatus;
// import org.springframework.http.ResponseEntity;
// import com.xs.heart.service.HeartMessageBoxService;
// import com.xs.heart.entity.HeartMessageBox;
// import org.springframework.beans.factory.annotation.Autowired;
//
// /**
//  * <p>
//  * 好友消息盒子表 前端控制器
//  * </p>
//  *
//  * @author
//  * @since 2024-04-03
//  */
// @Controller
// @RequestMapping("/heart-message-box")
// public class HeartMessageBoxAction {
//
//
//     @Autowired
//     private HeartMessageBoxService heartMessageBoxService;
//
//     @GetMapping(value = "/")
//     public ResponseEntity<Page<HeartMessageBox>> list(@RequestParam(required = false) Integer current, @RequestParam(required = false) Integer pageSize) {
//         if (current == null) {
//             current = 1;
//         }
//         if (pageSize == null) {
//             pageSize = 10;
//         }
//         Page<HeartMessageBox> aPage = heartMessageBoxService.page(new Page<>(current, pageSize));
//         return new ResponseEntity<>(aPage, HttpStatus.OK);
//     }
//
//     @GetMapping(value = "/{id}")
//     public ResponseEntity<HeartMessageBox> getById(@PathVariable("id") String id) {
//         return new ResponseEntity<>(heartMessageBoxService.getById(id), HttpStatus.OK);
//     }
//
//     @PostMapping(value = "/create")
//     public ResponseEntity<Object> create(@RequestBody HeartMessageBox params) {
//         heartMessageBoxService.save(params);
//         return new ResponseEntity<>("created successfully", HttpStatus.OK);
//     }
//
//     @PostMapping(value = "/delete/{id}")
//     public ResponseEntity<Object> delete(@PathVariable("id") String id) {
//         heartMessageBoxService.removeById(id);
//         return new ResponseEntity<>("deleted successfully", HttpStatus.OK);
//     }
//
//     @PostMapping(value = "/update")
//     public ResponseEntity<Object> update(@RequestBody HeartMessageBox params) {
//         heartMessageBoxService.updateById(params);
//         return new ResponseEntity<>("updated successfully", HttpStatus.OK);
//     }
// }
