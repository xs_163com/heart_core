package com.xs.heart.mapper;

import com.xs.heart.entity.HeartMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户聊天详细内容表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
public interface HeartMessageMapper extends BaseMapper<HeartMessage> {

}
