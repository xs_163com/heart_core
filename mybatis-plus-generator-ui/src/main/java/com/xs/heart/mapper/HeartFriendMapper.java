package com.xs.heart.mapper;

import com.xs.heart.entity.HeartFriend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 好友关系表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
public interface HeartFriendMapper extends BaseMapper<HeartFriend> {

}
