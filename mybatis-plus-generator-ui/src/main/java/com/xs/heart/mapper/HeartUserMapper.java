package com.xs.heart.mapper;

import com.xs.heart.entity.HeartUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
public interface HeartUserMapper extends BaseMapper<HeartUser> {

}
