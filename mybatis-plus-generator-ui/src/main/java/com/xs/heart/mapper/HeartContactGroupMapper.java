package com.xs.heart.mapper;

import com.xs.heart.entity.HeartContactGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 好友分组表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
public interface HeartContactGroupMapper extends BaseMapper<HeartContactGroup> {

}
