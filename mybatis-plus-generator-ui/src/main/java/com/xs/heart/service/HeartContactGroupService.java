package com.xs.heart.service;

import com.xs.heart.entity.HeartContactGroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 好友分组表 服务类
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
public interface HeartContactGroupService extends IService<HeartContactGroup> {

}
