package com.xs.heart.service.impl;

import com.xs.heart.entity.HeartFriend;
import com.xs.heart.mapper.HeartFriendMapper;
import com.xs.heart.service.HeartFriendService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 好友关系表 服务实现类
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
@Service
public class HeartFriendServiceImpl extends ServiceImpl<HeartFriendMapper, HeartFriend> implements HeartFriendService {

}
