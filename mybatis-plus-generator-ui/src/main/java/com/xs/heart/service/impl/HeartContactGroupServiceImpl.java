package com.xs.heart.service.impl;

import com.xs.heart.entity.HeartContactGroup;
import com.xs.heart.mapper.HeartContactGroupMapper;
import com.xs.heart.service.HeartContactGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 好友分组表 服务实现类
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
@Service
public class HeartContactGroupServiceImpl extends ServiceImpl<HeartContactGroupMapper, HeartContactGroup> implements HeartContactGroupService {

}
