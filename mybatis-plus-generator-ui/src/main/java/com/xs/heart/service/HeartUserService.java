package com.xs.heart.service;

import com.xs.heart.entity.HeartUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
public interface HeartUserService extends IService<HeartUser> {

}
