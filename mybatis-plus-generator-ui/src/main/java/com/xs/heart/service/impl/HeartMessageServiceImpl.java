package com.xs.heart.service.impl;

import com.xs.heart.entity.HeartMessage;
import com.xs.heart.mapper.HeartMessageMapper;
import com.xs.heart.service.HeartMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户聊天详细内容表 服务实现类
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
@Service
public class HeartMessageServiceImpl extends ServiceImpl<HeartMessageMapper, HeartMessage> implements HeartMessageService {

}
