package com.xs.heart.service;

import com.xs.heart.entity.HeartMessage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户聊天详细内容表 服务类
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
public interface HeartMessageService extends IService<HeartMessage> {

}
