package com.xs.heart.service;

import com.xs.heart.entity.HeartFriend;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 好友关系表 服务类
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
public interface HeartFriendService extends IService<HeartFriend> {

}
