package com.xs.heart.service.impl;

import com.xs.heart.entity.HeartUser;
import com.xs.heart.mapper.HeartUserMapper;
import com.xs.heart.service.HeartUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author 
 * @since 2024-04-03
 */
@Service
public class HeartUserServiceImpl extends ServiceImpl<HeartUserMapper, HeartUser> implements HeartUserService {

}
