package com.xs.heart.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 好友分组表
 * </p>
 *
 * @author
 * @since 2024-04-03
 */
@Getter
@Setter
@TableName("heart.heart_contact_group")
public class HeartContactGroup extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId("id")
    private Integer id;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private String userId;

    /**
     * 分组名称
     */
    @TableField("name")
    private String name;

    /**
     * 好友数
     */
    @TableField("num")
    private String num;

    /**
     * 排序
     */
    @TableField("sort")
    private String sort;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;
}
