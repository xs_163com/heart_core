package com.xs.heart.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 好友申请表
 * </p>
 *
 * @author
 * @since 2024-04-10
 */
@Getter
@Setter
@TableName("heart.heart_contact_apply")
public class HeartContactApply extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     * 申请ID主键
     */
    @TableId("id")
    private Integer id;

    /**
     * 申请人ID
     */
    @TableField("user_id")
    private String userId;

    /**
     * 被申请人ID
     */
    @TableField("friend_id")
    private String friendId;

    /**
     * 申请备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 是否同意（1 已同意 2拒绝 3待处理）
     */
    @TableField("agree")
    private String agree;

    /**
     * 申请时间（创建时间）
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 同意时间
     */
    @TableField("agree_time")
    private Date agreeTime;

    /**
     * 1 未删除 2 已删除
     */
    @TableField("deleted")
    @TableLogic
    private String deleted;
}
