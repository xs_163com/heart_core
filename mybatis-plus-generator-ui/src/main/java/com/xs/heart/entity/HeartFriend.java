package com.xs.heart.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 好友关系表
 * </p>
 *
 * @author
 * @since 2024-04-03
 */
@Getter
@Setter
@TableName("heart.heart_friend")
public class HeartFriend extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private String userId;

    /**
     * 朋友id
     */
    @TableField("friend_id")
    private String friendId;

    /**
     * 逻辑删除
     */
    @TableField("deleted")
    // @TableLogic TODO 之后再配置默认的逻辑删除
    private String deleted;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 好友状态 [1:否2:是]
     */
    @TableField("status")
    private String status;

    /**
     * 分组ID
     */
    @TableField("group_id")
    private Integer groupId;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;
}
