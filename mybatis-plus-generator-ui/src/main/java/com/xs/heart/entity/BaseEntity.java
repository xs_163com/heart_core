package com.xs.heart.entity;

import java.io.Serializable;


/**
 * 基础类
 * @author xs
 */
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
}
