package com.xs.heart.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户聊天详细内容表
 * </p>
 *
 * @author
 * @since 2024-04-03
 */
@Getter
@Setter
@TableName("heart.heart_message")
public class HeartMessage extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     * 消息的主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 消息
     */
    @TableField("content")
    private String content;

    /**
     * 消息类型(1：文本 2：图片 3：表情)
     */
    @TableField("message_type")
    private String messageType;

    /**
     * 消息id
     */
    @TableField("message_id")
    private String messageId;

    /**
     * 属于那个用户的消息
     */
    @TableField("user_id")
    private String userId;

    /**
     * 接收消息用户id
     */
    @TableField("receiver_id")
    private String receiverId;

    /**
     * 消息id
     */
    @TableField("msg_id")
    private String msgId;

    /**
     * 1单聊  2群聊
     */
    @TableField("talk_type")
    private String talkType;

    /**
     * 是否撤回[0:否;1:是;]
     */
    @TableField("is_revoke")
    private String isRevoke;

    /**
     * 消息扩展字段
     */
    @TableField("extra")
    private String extra;

    /**
     * 逻辑删除(1 未删除 2，已删除)
     */
    @TableField("deleted")
    // @TableLogic TODO 之后再配置默认的逻辑删除
    private String deleted;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 消息盒子主键ID
     */
    @TableField("msg_box_id")
    private Long msgBoxId;
}
