package com.xs.heart.config;

import com.xs.heart.interceptor.HeartUserInfoAuthInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author xs
 */
@Configuration
//因为 HandlerInterceptor 是 springmvc的，所以，网关引入之后会找不到该文件，但是又因为其他微服务有boot-stater，有mvc所以可以引用
// 这样他就可以不让网关引入了
@ConditionalOnClass(DispatcherServlet.class)
public class MvcConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new HeartUserInfoAuthInterceptor());
        WebMvcConfigurer.super.addInterceptors(registry);
    }
}
