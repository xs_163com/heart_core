package com.xs.heart.interceptor;


import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.xs.heart.entity.User;
import com.xs.heart.entity.vo.UserTokenVO;
import com.xs.heart.utils.AuthContextUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * @author xs
 */
public class HeartUserInfoAuthInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //1 获取登录用户信息
        String userInfo = request.getHeader("userInfo");



        // 2 判断是否获取了用户，如果有，存入ThreadLocal
        if (null != userInfo && !userInfo.isEmpty()){
            UserTokenVO userTokenVO = JSONObject.parseObject(userInfo, UserTokenVO.class);
            //User user = new User();
            //user.setUserId(userInfo);
            AuthContextUtil.setThreadLocal(userTokenVO);
        }

        //3 放行
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        AuthContextUtil.deleteThreadLocal();
    }
}
