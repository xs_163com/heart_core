package com.xs.heart.entity.vo;

import com.xs.heart.entity.BaseEntity;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserTokenVO extends BaseEntity {

    private String userId;
    private String username;
    private String nickName;
    private String phoneNumber;
    private String gender;
    private String photoImgUrl;
    private String addr;
    private String signature;
}
