package com.xs.heart.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Megtob
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("heart_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 用户id,唯一主键
     */
    @TableId("user_id")
    private String userId;

    /**
     * 账号（唯一）
     */
    @TableField("user_name")
    private String username;

    /**
     * 用户昵称
     */
    @TableField("nick_name")
    private String nickName;

    /**
     * 电话号码
     */
    @TableField("phone_number")
    private String phoneNumber;

    /**
     * 性别（0：女；1：男）
     */
    @TableField("gender")
    private String gender;

    /**
     * 未加密密码
     */
    @TableField("password")
    private String password;

    /**
     * 加密密码
     */
    @TableField("encrypt_password")
    private String encryptPassword;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 头像的文件地址
     */
    @TableField("photo_img_url")
    private String photoImgUrl;

    /**
     * 城市
     */
    @TableField("addr")
    private String addr;

    /**
     * 个性签名
     */
    @TableField("signature")
    private String signature;

    /**
     * 逻辑删除（1：未删除，2：已删除）
     */
    @TableField("deleted")
    // @TableLogic TODO 之后再配置默认的逻辑删除
    private String deleted;

}
