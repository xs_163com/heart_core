package com.xs.heart.utils;


import com.xs.heart.exception.XsExceptionHandler;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 *
 * @author gientech
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils
{
    public static long DAY_MS = 1000 * 60 * 60 * 24;

    public static String YYYY = "yyyy";

    public static String YYYY_MM = "yyyy-MM";

    public static String YYYY_MM_DD = "yyyy-MM-dd";

    public static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    public static String YYYY_MM_DD_HH_MM_SS_SLASH = "yyyy/MM/dd HH:mm:ss";
    public static String YYYY_MM_DD_HH_MM_SS_SLASH_PATTERN = "\\d{4}/\\d{2}/\\d{2} \\d{2}:\\d{2}:\\d{2}";

    public static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};


    /**
     * 获取当前Date型日期
     *
     * @return Date() 当前日期
     */
    public static Date getNowDate()
    {
        return new Date();
    }

    /**
     * 获取当前日期, 默认格式为yyyy-MM-dd
     *
     * @return String
     */
    public static String getDate()
    {
        return dateTimeNow(YYYY_MM_DD);
    }

    public static final String getTime()
    {
        return dateTimeNow(YYYY_MM_DD_HH_MM_SS);
    }

    public static final String dateTimeNow()
    {
        return dateTimeNow(YYYYMMDDHHMMSS);
    }

    public static final String dateTimeNow(final String format)
    {
        return parseDateToStr(format, new Date());
    }

    public static final String dateTime(final Date date)
    {
        return parseDateToStr(YYYY_MM_DD, date);
    }

    public static final String parseDateToStr(final String format, final Date date)
    {
        return new SimpleDateFormat(format).format(date);
    }

    public static final Date parseStrToDate(final String timeStr,final String format)
    {
        try {
            return new SimpleDateFormat(format).parse(timeStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static final Date dateTime(final String format, final String ts)
    {
        try
        {
            return new SimpleDateFormat(format).parse(ts);
        }
        catch (ParseException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static final String getYear(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return String.valueOf(calendar.get(Calendar.YEAR));
    }

    /**
     * 日期路径 即年/月/日 如2018/08/08
     */
    public static final String datePath()
    {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyy/MM/dd");
    }

    /**
     * 日期路径 即年/月/日 如20180808
     */
    public static final String dateTime()
    {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyyMMdd");
    }

    /**
     * 日期型字符串转化为日期 格式
     */
    public static Date parseDate(Object str)
    {
        if (str == null)
        {
            return null;
        }
        try
        {
            return parseDate(str.toString(), parsePatterns);
        }
        catch (ParseException e)
        {
            return null;
        }
    }

    /**
     * 获取服务器启动时间
     */
    public static Date getServerStartDate()
    {
        long time = ManagementFactory.getRuntimeMXBean().getStartTime();
        return new Date(time);
    }

    /**
     * 计算两个时间差
     */
    public static String getDatePoor(Date endDate, Date nowDate)
    {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return Math.abs(day) + "天" + Math.abs(hour) + "小时" + Math.abs(min) + "分钟";
    }

    public static String getDatePoor(Long endDate, Long nowDate)
    {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate - nowDate;
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return Math.abs(day) + "天" + Math.abs(hour) + "小时" + Math.abs(min) + "分钟";
    }
    /**
     * 计算两个时间差多少天，减去毫秒值偏移量
     */
    public static String getDiffDayOffSetTime(Date endDate, Date nowDate, long offSetTime)
    {
        offSetTime = StringUtils.isEmpty(offSetTime) ? 0l : offSetTime;
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime() - offSetTime;
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        return day + "天" + hour + "小时";
    }

    /**
     * 计算两个时间差,格式：时：分：秒
     * @param endDate 结束时间
     * @param nowDate 开始时间
     * @return 时：分：秒   例：905:16:3
     */
    public static String getHourTimePoor(Date endDate, Date nowDate)
    {
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少小时
        long hour = diff / nh;
        // 计算差多少分钟
        long min = diff % nh / nm;
         //计算差多少秒//输出结果
         long sec = diff % nh % nm / ns;
        return hour + ":" + min + ":" + sec;
    }

    /**
     * 计算两个时间差-分钟
     * @param endDate 结束时间
     * @param nowDate 开始时间
     * @return 时：   例：905小时
     */
    public static Long getMinuteTimePoor(Long endDate, Long nowDate)
    {
        long nh = 1000 * 60 ;
        // 获得两个时间的毫秒时间差异
        long diff = endDate - nowDate;
        // 计算差多少小时
        long minute = diff / nh;
        return Math.abs(minute) ;
    }

    /**
     * 获取指定时间的偏移时间
     * @param date 指定时间
     * @param offset  偏移值
     * @return 时间
     */
    public static String getDateOffsetDay(Date date, int offset)
    {
        Calendar calendar = Calendar.getInstance(); //得到日历
        calendar.setTime(date);//把当前时间赋给日历
        calendar.add(Calendar.DATE, offset);  //前推几天
        return DateFormatUtils.format(calendar.getTime(), YYYY_MM_DD);
    }

    /**
     * @Description 获取oozie调度任务开始时间，默认为当天凌晨1点
     * @Author yangjie
     * Date: 2021/4/22 16:00
     * @return String
     **/
    public static String getOozieStartTime()
    {
        return getDate()+" 01:00:00";
    }

    /**
     * @Description 获取oozie调度任务结束时间，默认为"2099-12-31 23:59:59"
     * @Author yangjie
     * Date: 2021/4/22 16:00
     * @return String
     **/
    public static String getOozieEndTime()
    {
        return "2099-12-31 23:59:59";
    }
    public static Date getRandomDate(Date start,Date end) {
        Long startL = start.getTime();
        Long endL = end.getTime();
        Long middleL = (long)(startL + (endL-startL)*Math.random());
        return new Date(middleL);
    }
    public static Date getRandomDate() {
        Date middleTime = new Date();
        try {
            Date start = DateUtils.parseDate("2020-01-12 00:00:58","yyyy-MM-dd HH:mm:ss");
            Date end= new Date();
            middleTime = getRandomDate(start,end);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return middleTime;
    }
    public static String getRandomDateStr() {
        Date middleTime = getRandomDate();
        String formatDate = DateFormatUtils.format(middleTime, YYYY_MM_DD);
        return formatDate;
    }

    /**
     * 计算相差天数
     */
    public static Long calculateDiffDay(Date startTime,Date endTime){
        Long start = startTime.getTime();
        Long end = endTime.getTime();
        Long day = (start - end) / 1000 / 3600 / 24;
        return Math.abs(day);
    }

    /**
     * 计算相差天数至ms
     */
    public static Long getDiffDayToMs(Date startTime, Date endTime){
        Long start = startTime.getTime();
        Long end = endTime.getTime();
        return  Math.abs(start - end);
    }
    /**
     * 相差天数计算
     */
    public static int differentDaysByMillisecond(Date now, Date endDate) {
        if (endDate.getTime()-now.getTime()<=((1000 * 3600 * 24 * 3))){
            //小于三天显示红色
            return 3;
        }else if (endDate.getTime()-now.getTime()<=((1000 * 3600 * 24 * 7))){
            return 2;
            //3-7天显示黄色
        }else {
            //大于7天正常显示
            return 1;
        }
    }
    /**
     * 判断给定日期是否是当月的最后一天
     * @param date
     * @return
     */
    public static boolean isLastDayOfMonth(Date date) {
        //1、创建日历类
        Calendar calendar = Calendar.getInstance();
        //2、设置当前传递的时间，不设就是当前系统日期
        calendar.setTime(date);
        //3、data的日期是N，那么N+1【假设当月是30天，30+1=31，如果当月只有30天，那么最终结果为1，也就是下月的1号】
        calendar.set(Calendar.DATE, (calendar.get(Calendar.DATE) + 1));
        //4、判断是否是当月最后一天【1==1那么就表明当天是当月的最后一天返回true】
        return calendar.get(Calendar.DAY_OF_MONTH) == 1 ? true : false;
    }

    /**
     * 统一格式化日期
     * @param dateString
     * @return 2023-05-23 12:12:56
     */
    public static String addDateTimeZero(String dateString) {
        if (StringUtils.isEmpty(dateString)){
            return null;
        }
        dateString = dateString.replaceAll("T"," ");
        String[] s = dateString.split(" ");
        if (StringUtils.isEmpty(s)){
            return null;
        }

        if (s.length == 2){
            String s1 = s[1];
            if (s1.length() == 2) {
                return s[0] + " " + s1 + ":00:00";
            }
            else if (s1.length() == 5) {
                return s[0] + " " + s1 + ":00";
            }
            else if (s1.length() == 8){
                return s[0] + " " + s1;
            }

        }
        return s[0] + " " + "00:00:00";
    }

    /**
     * 计算两个日期之间的相差天数  例: 2023-11-01 扫到 2023-10-31就是1
     *
     * @param sourceDate 开始时间
     * @param nowDate 结束时间
     * @return
     */
    public static long getDayToDay(Date sourceDate,Date nowDate) {
        if (null == nowDate || null ==sourceDate ){
            throw new XsExceptionHandler("传入时间不能为空",500);
        }
        LocalDate ct1 = sourceDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate nowDate1 = nowDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        return Math.abs(ChronoUnit.DAYS.between(ct1, nowDate1));
    }




    /**
     * 计算两个日期之间的相差天数  例: 2023-11-01 扫到 2023-10-31就是 2
     * 特殊要求 不同场景使用
     *
     * @param sDate 开始时间
     * @param eDate 结束时间
     * @return
     */
    public static long getDayToDayPlus(Date sDate,Date eDate) {
        return getDayToDay(sDate, eDate) + 1;
    }


}
