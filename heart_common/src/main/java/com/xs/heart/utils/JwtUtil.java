package com.xs.heart.utils;

import com.xs.heart.entity.vo.UserTokenVO;
import io.jsonwebtoken.*;
// import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class JwtUtil {

    public static final Long time =  3600000L;

    private static String sign = "Heart";
    public static String generateJwt(UserTokenVO userTokenVO){
        JwtBuilder builder = Jwts.builder();
        String token = builder.setHeaderParam("typ","JWT")
                .setHeaderParam("alg","HS256")
                .claim("userTokenVO",userTokenVO)
                .setSubject("heart-jwt")
                .setExpiration(new Date(System.currentTimeMillis()+time))
                .setId(UUID.randomUUID().toString().replaceAll("-",""))
                .signWith(SignatureAlgorithm.HS256,sign)
                .compact();
        System.out.println(token);
        return token;
    }
    public static Claims parseJwt(String token){
        System.out.println(token);
        JwtParser parser = Jwts.parser();
        Jws<Claims> claimsJws = parser.setSigningKey(sign).parseClaimsJws(token);
        Claims body = claimsJws.getBody();

        System.out.println(body);
        return body;
    }
}
