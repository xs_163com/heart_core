package com.xs.heart.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 *
 * @author gientech
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils
{
    private static Logger logger = LogManager.getLogger(StringUtils.class);
    /** 空字符串 */
    private static final String NULLSTR = "";

    /** 下划线 */
    private static final char SEPARATOR = '_';

    /** 星号 */
    private static final String START = "*";

    /**
     * 获取参数不为空值
     *
     * @param value defaultValue 要判断的value
     * @return value 返回值
     */
    public static <T> T nvl(T value, T defaultValue)
    {
        return value != null ? value : defaultValue;
    }

    /**
     * * 判断一个Collection是否为空， 包含List，Set，Queue
     *
     * @param coll 要判断的Collection
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(Collection<?> coll)
    {
        return isNull(coll) || coll.isEmpty();
    }

    /**
     * * 判断一个Collection是否非空，包含List，Set，Queue
     *
     * @param coll 要判断的Collection
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Collection<?> coll)
    {
        return !isEmpty(coll);
    }

    /**
     * * 判断一个对象数组是否为空
     *
     * @param objects 要判断的对象数组
     ** @return true：为空 false：非空
     */
    public static boolean isEmpty(Object[] objects)
    {
        return isNull(objects) || (objects.length == 0);
    }

    /**
     * * 判断一个对象数组是否非空
     *
     * @param objects 要判断的对象数组
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Object[] objects)
    {
        return !isEmpty(objects);
    }

    /**
     * * 判断一个Map是否为空
     *
     * @param map 要判断的Map
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(Map<?, ?> map)
    {
        return isNull(map) || map.isEmpty();
    }

    /**
     * * 判断一个Map是否为空
     *
     * @param map 要判断的Map
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Map<?, ?> map)
    {
        return !isEmpty(map);
    }

    /**
     * * 判断一个字符串是否为空串
     *
     * @param str String
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(String str)
    {
        return isNull(str) || NULLSTR.equals(str.trim());
    }

    /**
     * * 判断一个字符串是否为非空串
     *
     * @param str String
     * @return true：非空串 false：空串
     */
    public static boolean isNotEmpty(String str)
    {
        return !isEmpty(str);
    }

    /**
     * * 判断一个对象是否为空
     *
     * @param object Object
     * @return true：为空 false：非空
     */
    public static boolean isNull(Object object)
    {
        return object == null;
    }

    /**
     * * 判断一个对象是否非空
     *
     * @param object Object
     * @return true：非空 false：空
     */
    public static boolean isNotNull(Object object)
    {
        return !isNull(object);
    }

    /**
     * * 判断一个对象是否是数组类型（Java基本型别的数组）
     *
     * @param object 对象
     * @return true：是数组 false：不是数组
     */
    public static boolean isArray(Object object)
    {
        return isNotNull(object) && object.getClass().isArray();
    }

    /**
     * 去空格
     */
    public static String trim(String str)
    {
        return (str == null ? "" : str.trim());
    }

    /**
     * 截取字符串
     *
     * @param str 字符串
     * @param start 开始
     * @return 结果
     */
    public static String substring(final String str, int start)
    {
        if (str == null)
        {
            return NULLSTR;
        }

        if (start < 0)
        {
            start = str.length() + start;
        }

        if (start < 0)
        {
            start = 0;
        }
        if (start > str.length())
        {
            return NULLSTR;
        }

        return str.substring(start);
    }

    /**
     * 截取字符串
     *
     * @param str 字符串
     * @param start 开始
     * @param end 结束
     * @return 结果
     */
    public static String substring(final String str, int start, int end)
    {
        if (str == null)
        {
            return NULLSTR;
        }

        if (end < 0)
        {
            end = str.length() + end;
        }
        if (start < 0)
        {
            start = str.length() + start;
        }

        if (end > str.length())
        {
            end = str.length();
        }

        if (start > end)
        {
            return NULLSTR;
        }

        if (start < 0)
        {
            start = 0;
        }
        if (end < 0)
        {
            end = 0;
        }

        return str.substring(start, end);
    }



    /**
     * 下划线转驼峰命名
     */
    public static String toUnderScoreCase(String str)
    {
        if (str == null)
        {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        // 前置字符是否大写
        boolean preCharIsUpperCase = true;
        // 当前字符是否大写
        boolean curreCharIsUpperCase = true;
        // 下一字符是否大写
        boolean nexteCharIsUpperCase = true;
        for (int i = 0; i < str.length(); i++)
        {
            char c = str.charAt(i);
            if (i > 0)
            {
                preCharIsUpperCase = Character.isUpperCase(str.charAt(i - 1));
            }
            else
            {
                preCharIsUpperCase = false;
            }

            curreCharIsUpperCase = Character.isUpperCase(c);

            if (i < (str.length() - 1))
            {
                nexteCharIsUpperCase = Character.isUpperCase(str.charAt(i + 1));
            }

            if (preCharIsUpperCase && curreCharIsUpperCase && !nexteCharIsUpperCase)
            {
                sb.append(SEPARATOR);
            }
            else if ((i != 0 && !preCharIsUpperCase) && curreCharIsUpperCase)
            {
                sb.append(SEPARATOR);
            }
            sb.append(Character.toLowerCase(c));
        }

        return sb.toString();
    }

    /**
     * 是否包含字符串
     *
     * @param str 验证字符串
     * @param strs 字符串组
     * @return 包含返回true
     */
    public static boolean inStringIgnoreCase(String str, String... strs)
    {
        if (str != null && strs != null)
        {
            for (String s : strs)
            {
                if (str.equalsIgnoreCase(trim(s)))
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 将下划线大写方式命名的字符串转换为驼峰式。如果转换前的下划线大写方式命名的字符串为空，则返回空字符串。 例如：HELLO_WORLD->HelloWorld
     *
     * @param name 转换前的下划线大写方式命名的字符串
     * @return 转换后的驼峰式命名的字符串
     */
    public static String convertToCamelCase(String name)
    {
        StringBuilder result = new StringBuilder();
        // 快速检查
        if (name == null || name.isEmpty())
        {
            // 没必要转换
            return "";
        }
        else if (!name.contains("_"))
        {
            // 不含下划线，仅将首字母大写
            return name.substring(0, 1).toUpperCase() + name.substring(1);
        }
        // 用下划线将原始字符串分割
        String[] camels = name.split("_");
        for (String camel : camels)
        {
            // 跳过原始字符串中开头、结尾的下换线或双重下划线
            if (camel.isEmpty())
            {
                continue;
            }
            // 首字母大写
            result.append(camel.substring(0, 1).toUpperCase());
            result.append(camel.substring(1).toLowerCase());
        }
        return result.toString();
    }

    /**
     * 驼峰式命名法 例如：user_name->userName
     */
    public static String toCamelCase(String s)
    {
        if (s == null)
        {
            return null;
        }
        s = s.toLowerCase();
        StringBuilder sb = new StringBuilder(s.length());
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);

            if (c == SEPARATOR)
            {
                upperCase = true;
            }
            else if (upperCase)
            {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            }
            else
            {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * 查找指定字符串是否匹配指定字符串列表中的任意一个字符串
     *
     * @param str 指定字符串
     * @param strs 需要检查的字符串数组
     * @return 是否匹配
     */
    public static boolean matches(String str, List<String> strs)
    {
        if (isEmpty(str) || isEmpty(strs))
        {
            return false;
        }
        for (String testStr : strs)
        {
            if (matches(str, testStr))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 查找指定字符串是否匹配指定字符串数组中的任意一个字符串
     *
     * @param str 指定字符串
     * @param strs 需要检查的字符串数组
     * @return 是否匹配
     */
    public static boolean matches(String str, String... strs)
    {
        if (isEmpty(str) || isEmpty(strs))
        {
            return false;
        }
        for (String testStr : strs)
        {
            if (matches(str, testStr))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 查找指定字符串是否匹配
     *
     * @param str 指定字符串
     * @param pattern 需要检查的字符串
     * @return 是否匹配
     */
    public static boolean matches(String str, String pattern)
    {
        if (isEmpty(pattern) || isEmpty(str))
        {
            return false;
        }

        pattern = pattern.replaceAll("\\s*", ""); // 替换空格
        int beginOffset = 0; // pattern截取开始位置
        int formerStarOffset = -1; // 前星号的偏移位置
        int latterStarOffset = -1; // 后星号的偏移位置

        String remainingURI = str;
        String prefixPattern = "";
        String suffixPattern = "";

        boolean result = false;
        do
        {
            formerStarOffset = indexOf(pattern, START, beginOffset);
            prefixPattern = substring(pattern, beginOffset, formerStarOffset > -1 ? formerStarOffset : pattern.length());

            // 匹配前缀Pattern
            result = remainingURI.contains(prefixPattern);
            // 已经没有星号，直接返回
            if (formerStarOffset == -1)
            {
                return result;
            }

            // 匹配失败，直接返回
            if (!result) {
                return false;
            }

            if (!isEmpty(prefixPattern))
            {
                remainingURI = substringAfter(str, prefixPattern);
            }

            // 匹配后缀Pattern
            latterStarOffset = indexOf(pattern, START, formerStarOffset + 1);
            suffixPattern = substring(pattern, formerStarOffset + 1, latterStarOffset > -1 ? latterStarOffset : pattern.length());

            result = remainingURI.contains(suffixPattern);
            // 匹配失败，直接返回
            if (!result) {
                return false;
            }
            if (!isEmpty(suffixPattern))
            {
                remainingURI = substringAfter(str, suffixPattern);
            }

            // 移动指针
            beginOffset = latterStarOffset + 1;

        }
        while (!isEmpty(suffixPattern) && !isEmpty(remainingURI));

        return true;
    }

    @SuppressWarnings("unchecked")
    public static <T> T cast(Object obj)
    {
        return (T) obj;
    }

    /**
     * 获取下一个版本，每次增加0.01
     * @param curVersion
     * @return
     */
    public static String getNextVersion(String curVersion){
        String nextVersion = curVersion;
        if(isNotEmpty(curVersion)) {
            String[] split = curVersion.split("V");
            if (split.length == 2) {
                BigDecimal versionNum = new BigDecimal(split[1]);
                BigDecimal add = versionNum.add(BigDecimal.valueOf(0.01));//一次增加0.01
                nextVersion = "V" + add.toString();
            }else {
                logger.warn("编码请以V开头，如V1.0");
            }
        }
        return nextVersion;
    }

    /**
     * 判断是否是整数
     * @param str
     * @return
     */
    public static boolean isInteger(String str) {
        if("-".equals(str)){
            return false;
        }
        boolean isInteger = false;
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        if(pattern.matcher(str).matches()){
            try {
                Integer.parseInt(str);
                isInteger = true;
            } catch (NumberFormatException e) {
                isInteger = false;
            }
        }
        return isInteger;
    }

    /**
     * 判断是否是正负、整数、小数
     * @param str
     * @return
     */
    public static boolean isNumber(String str) {
        Boolean strResult = str.matches("-?[0-9]+.*[0-9]*");
        return strResult;
    }



    /**
     * 判断object为空
     * @param obj
     * @return
     */
    public static boolean isEmpty(Object obj)
    {
        return isNull(obj) || isEmpty(String.valueOf(obj));
    }

    public static boolean isNotEmpty(Object obj)
    {
        return !isEmpty(obj);
    }

    /**
     * obj 转 list
     * @param obj 入参List类型的Object对象
     * @param clazz 要转换成的list中的类型
     * @param <T>
     * @return
     */
    public static <T> List<T> objToList(Object obj, Class<T> clazz){
        List<T> result = new ArrayList<>();
        if (obj instanceof List<?>){
            for (Object o: (List<?>)obj) {
                result.add(clazz.cast(o));
            }
            return result;
        }
        return null;
    }

    /**
     * 判断字符是否是中文
     * @param c 字符串
     * @return 是否是中文
     */
    public static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS //CJK 中日韩统一表意符号
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS //CJK 兼容象形文字
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A //CJK 统一表意符号扩展A
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION //常用标点
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION //CJK 符号和标点
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS //半角及全角形式
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_FORMS // (主要是给竖写方式使用的括号，以及间断线﹉，波浪线﹌等)
                || ub == Character.UnicodeBlock.VERTICAL_FORMS) {  // (主要是一些竖着写的标点符号，    等等)
            return true;
        }
        return false;
    }


    // 根据UnicodeBlock方法判断中文标点符号
    public boolean isChinesePunctuation(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.GENERAL_PUNCTUATION // (百分号，千分号，单引号，双引号等)
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION //( 顿号，句号，书名号，〸，〹，〺 等；PS: 后面三个字符你知道什么意思吗？ : )    )
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS // ( 大于，小于，等于，括号，感叹号，加，减，冒号，分号等等)
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_FORMS // (主要是给竖写方式使用的括号，以及间断线﹉，波浪线﹌等)
                || ub == Character.UnicodeBlock.VERTICAL_FORMS) { // (主要是一些竖着写的标点符号，    等等)
            return true;
        } else {
            return false;
        }
    }

    public static boolean isEnglish(char c) {
        if ((c >= 97 && c <= 122) || (c>= 65 && c <= 90)) {
            return true;
        }
        return false;
    }

    public static boolean isNumber(char c) {
        return Character.isDigit(c);
    }

    /**
     * object对象转json字符串
     * @param obj 对象
     * @return String json字符串
     */
    @SneakyThrows
    public static String parseJsonStr(Object obj){
        ObjectMapper mapper = new ObjectMapper();
        String jsonStr = null != obj ? mapper.writeValueAsString(obj) : "";
        return jsonStr;
    }

    /**
     * 16进制转字符串
     * @param hex 16进制。例：传单个【7C:】，传多个【23:25:7C:】
     * @param charsetName 字符集名称，可以为空null，默认utf-8
     * @return 返回16进制对应的字符串：例：|、#%|
     */
    public static String hexToString(String hex, String charsetName) {
        if (StringUtils.isEmpty(charsetName))
            charsetName = "utf-8";
        String result = hex;
        if (!(hex.indexOf(":") == -1)) {
            String[] temps = hex.split(":");
            byte[] bs = new byte[temps.length];
            try {
                for (int j = 0; j < temps.length; j++) {
                    // 转换byte[]
                    bs[j] = (byte) Integer.parseInt(temps[j], 16);
                }
                // 转换String,这里编码是utf-8,可设置,根据发snmp的编码集来
                // result = new String(bs, "utf-8");  gb2312、unicode
                result = new String(bs, charsetName);
            } catch (Exception e) {
                // 一些字符不能转16进制，返回原内容
                return result;
            }
        }
        return result;
    }

}
