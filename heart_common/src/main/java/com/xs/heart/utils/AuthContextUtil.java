package com.xs.heart.utils;

import com.xs.heart.entity.User;
import com.xs.heart.entity.vo.UserTokenVO;
import org.springframework.stereotype.Component;


public class AuthContextUtil {


    /**
     * 创建threadlocal对象
     *
     * */
    private static final ThreadLocal<UserTokenVO> THREAD_LOCAL = new ThreadLocal<>();

    /**
    * 添加数据
    *
    * */
    public static void setThreadLocal(UserTokenVO user){
        THREAD_LOCAL.set(user);
    }
    /**
     * 获取数据
     *
     * */
    public static UserTokenVO getLoginUser(){
        return THREAD_LOCAL.get();
    }

    /**
     * 删除数据
     *
     * */
    public static void deleteThreadLocal(){
        THREAD_LOCAL.remove();
    }

}
