package com.xs.heart.utils;


import lombok.Data;


/**
 * @author xs
 */

@Data
public class Result<T> {

    private Integer code;

    private String message;

    private T data;

    private String token;

    //构造私有化 外面就不能new对象了
    private Result() {
    }

    /**
     * 所有参数
     * @param data
     * @param resultCodeEnum
     * @return
     * @param <T>
     */
    public  static<T> Result<T> build(T data , ResultCodeEnum resultCodeEnum ){

        Result<T> result = new Result<>();
        //判断返回结果中是否需要data数据
        if (null != data){
            result.setData(data);
        }
        //设置其他值
        result.setCode(resultCodeEnum.getCode());
        result.setMessage(resultCodeEnum.getMessage());
        return result;
    }

    /**
     * 所有参数
     * @param resultCodeEnum
     * @param resultCodeEnum
     * @return
     * @param <T>
     */
    public  static<T> Result<T> build(ResultCodeEnum resultCodeEnum ){

        Result<T> result = new Result<>();
        //判断返回结果中是否需要data数据
        //设置其他值
        result.setCode(resultCodeEnum.getCode());
        result.setMessage(resultCodeEnum.getMessage());
        return result;
    }



    /**
     * 传信息和code
     *
     * @param message
     * @return
     * @param <T>
     */
    public  static<T> Result<T> build(String message , Integer code){
        Result<T> result = new Result<>();
        result.setMessage(message);
        result.setCode(code);
        return result;
    }

    /**
     * 只有信息
     *
     * @param message
     * @return
     * @param <T>
     */
    public  static<T> Result<T> build(String message){
        Result<T> result = new Result<>();
        result.setMessage(message);
        return result;
    }


    /**
     * 只有code
     *
     * @param code
     * @return
     * @param <T>
     */
    public  static<T> Result<T> build(Integer code){
        Result<T> result = new Result<>();
        result.setCode(code);
        return result;
    }


    /**
     * 登录成功的返回方法
     *
     * @param
     * @return
     * @param <T>
     */
    public  static<T> Result<T> loginSuccess(String token,String message){
        Result<T> build = loginBuild(token, message);
        return build;
    }


    /**
     * 所有参数
     * @param
     * @param
     * @return
     * @param <T>
     */
    public  static<T> Result<T> loginBuild(String token,String message){

        Result<T> result = new Result<>();
        //判断返回结果中是否需要data数据
        //设置其他值
        result.setCode(ResultCodeEnum.SUCCESS.getCode());
        result.setMessage(message);
        result.setToken(token);
        return result;
    }

    /**
     * 成功的返回方法
     *
     * @param data
     * @return
     * @param <T>
     */
    public  static<T> Result<T> success(T data ){
        Result<T> build = build(data, ResultCodeEnum.SUCCESS);
        return build;
    }

    /**
     * 成功的返回方法
     *
     * @return
     * @param <T>
     */
    public  static<T> Result<T> success(){
        Result<T> build = build(ResultCodeEnum.SUCCESS);
        return build;
    }


    /**
     * 失败的返回方法
     *
     * @param data
     * @return
     * @param <T>
     */
    public  static<T> Result<T> fail(T data) {
        Result<T> build = build(data, ResultCodeEnum.FAIL);
        return build;
    }


    /**
     * 失败的返回方法
     *
     * @return
     * @param <T>
     */
    public  static<T> Result<T> fail() {
        Result<T> build = build( ResultCodeEnum.FAIL);
        return build;
    }


}
