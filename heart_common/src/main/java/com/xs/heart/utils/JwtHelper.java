package com.xs.heart.utils;


import io.jsonwebtoken.*;

import java.util.Date;
import java.util.UUID;

/**
 * @FileName: JwtHelper
 * @Author: zhaoxinguo
 * @Date: 2018/12/10 19:39
 * @Description: 实现Jwt
 */
public class JwtHelper {
public static final Long time =  3600000L;

private static String sign = "Heart";
    public static String test1(){
        JwtBuilder builder = Jwts.builder();
        String token = builder.setHeaderParam("typ","JWT")
                .setHeaderParam("alg","HS256")
                .claim("username","tom")
                .claim("role","admin")
                .setSubject("admin-test")
                .setExpiration(new Date(System.currentTimeMillis()+time))
                .setId(UUID.randomUUID().toString().replaceAll("-",""))
                .signWith(SignatureAlgorithm.HS256,sign)
                .compact();
        System.out.println(token);
        return token;
    }
    public static void parseTest(String token){
        System.out.println(token);
        JwtParser parser = Jwts.parser();
        Jws<Claims> claimsJws = parser.setSigningKey(sign).parseClaimsJws(token);
        Claims body = claimsJws.getBody();
        System.out.println(body);
    }

    //
    // public static void main(String[] args) {
    //     String token = test1();
    //     parseTest( token);
    // }
}
