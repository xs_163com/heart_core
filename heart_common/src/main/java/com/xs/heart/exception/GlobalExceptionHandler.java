package com.xs.heart.exception;


import com.xs.heart.utils.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author xs
 */

//面向切面
@ControllerAdvice
public class GlobalExceptionHandler {


    /**
     *全局异常处理
     * @param  @ExceptionHandler(Exception.class) 异常处理器
     * @param  @ResponseBody            返回json
     */
    @ExceptionHandler(Exception.class) //异常处理器
    @ResponseBody //返回json
    public Result error(Exception e){
        e.printStackTrace();
        return Result.fail(null);
    }



    /**
     *  自定义异常处理
     * @param  @ExceptionHandler(Exception.class) 异常处理器
     * @param  @ResponseBody            返回json
     */
    @ExceptionHandler(XsExceptionHandler.class) //异常处理器
    @ResponseBody //返回json
    public Result error(XsExceptionHandler e){
        e.printStackTrace();
        return Result.fail(null);
    }

}
