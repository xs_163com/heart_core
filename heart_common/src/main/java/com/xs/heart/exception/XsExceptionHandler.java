package com.xs.heart.exception;


import com.xs.heart.utils.ResultCodeEnum;
import lombok.Data;

/**
 *
 * 自定义异常类
 * @author xs
 */
@Data
public class XsExceptionHandler  extends RuntimeException{

    // private String message;
    // 异常状态码
    private Integer code;


    /**
     * 通过状态码和错误消息创建异常对象
     *
     * @param message
     * @param code
     */
    public XsExceptionHandler(String message,Integer code) {
        super(message);
        this.code = code;
    }

    /**
     * 接收自定义同意返回值枚举类
     *
     */
    public XsExceptionHandler(ResultCodeEnum resultCodeEnum){
        super(resultCodeEnum.getMessage());
        this.code =resultCodeEnum.getCode();
    }

    @Override
    public String toString() {
        return "GuliException{" +
                "code=" + code +
                ", message=" + this.getMessage() +
                '}';
    }
}
