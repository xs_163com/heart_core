package com.xs.demo.testDemo;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class ReceiveDemo {
    public static void main(String[] args) throws IOException {
        DatagramSocket socket = new DatagramSocket(6666);
        byte[] bytes = new byte[1024];

        DatagramPacket dp = new DatagramPacket(bytes, bytes.length);
        socket.receive(dp);
        byte[] data = dp.getData();
        System.out.println(data);
        int length = dp.getLength();
        System.out.println(length);
        InetAddress address = dp.getAddress();
        int port = dp.getPort();
        System.out.println(address);
        System.out.println(port);
        System.out.println(new String(data,0,length));
        

        socket.close();
    }
}
