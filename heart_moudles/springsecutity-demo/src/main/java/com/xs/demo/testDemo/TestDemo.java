package com.xs.demo.testDemo;

import java.net.*;

public class TestDemo {
    public static void main(String[] args) {
        try {
            DatagramSocket dSocket = new DatagramSocket();
            byte[] bytes = "你好".getBytes();
            InetAddress localhost = Inet4Address.getByName("localhost");
            int port = 6666;
            DatagramPacket ds = new DatagramPacket(bytes, bytes.length, localhost, port);
        //    发送数据
            dSocket.send(ds);
        //    释放资源
            dSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
