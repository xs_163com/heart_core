package com.xs.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

/**
 * @author xs
 */
@Configuration
@EnableWebSecurity  // 开启springSecurity的自定义配置（boot项目中可以省略）
public class WebSecurityConfig {

	/**
	 * security 的默认功能 学习
	 * @param http
	 * @return
	 * @throws Exception
	 */
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		//authorizeRequests()：开启授权保护
		http.authorizeRequests(authorize ->
						//对所有请求开启授权保护
						authorize.anyRequest()
						//已认证请求会自动被授权
						.authenticated()
				)
				.formLogin( form -> {
					form
							.loginPage("/login")
							.permitAll() //登录页面无需授权即可访问
							.usernameParameter("username") //自定义表单用户名参数，默认是username
							.passwordParameter("password") //自定义表单密码参数，默认是password
							.failureUrl("/login?error") //登录失败的返回地址
							.successHandler(new MyAuthenticationSuccessHandler())
							.failureHandler(new MyAuthenticationFailureHandler())
					;
				})//表单授权方式
				// .httpBasic(withDefaults())//基本授权方式
		;
		// 临时关闭csrf攻击
		// http.csrf(csrf->csrf.disable());

		/**
		 * 注销处理
		 */
		http.logout(logout -> {
			logout.logoutSuccessHandler(new MyLogoutSuccessHandler()); //注销成功时的处理
		});
		return http.build();
	}

	// @Bean
	// public UserDetailsService userDetailsService() {
	// 	//创建基于内存的用户信息管理器
	// 	InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
	// 	//创建UserDetail对象，用于管理用户名，用户密码，用户角色，用户权限等内容 当这个配完之后，配置文件的就已经失效了
	// 	manager.createUser(User.withDefaultPasswordEncoder().username("root").password("root").roles("USER").build());
	// 	return manager;
	// }

	// @Bean
	// public UserDetailsService userDetailsService() {
	// 	//创建基于数据库的用户信息管理器
	// 	DBUserDetailsManager manager = new DBUserDetailsManager();
	// 	return manager;
	// }
}
