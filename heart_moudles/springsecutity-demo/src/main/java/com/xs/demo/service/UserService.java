package com.xs.demo.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xs.demo.entity.User;
public interface UserService extends IService<User> {
    void saveUserDetails(User user);
}
