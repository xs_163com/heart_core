package com.xs.heart.controller;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.xs.heart.entity.UserDao;
import com.xs.heart.entity.vo.UserLoginVO;
import com.xs.heart.entity.vo.UserTokenVO;
import com.xs.heart.utils.AuthContextUtil;
import com.xs.heart.utils.JwtUtil;
import com.xs.heart.utils.Result;
import com.xs.heart.entity.User;
import com.xs.heart.service.UserService;
import com.xs.heart.utils.ResultCodeEnum;
import io.jsonwebtoken.Claims;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.security.SecureRandom;
import java.util.Base64;

/**
 * @author Megtob
 */
@RestController
@RequestMapping("/heart/admin/login")
public class LoginController {


    @Resource
    private UserService userService;

    @PostMapping("/registerUser")
    public Result registerUser(@RequestBody UserDao userDao){

        //先校验数据库中是否有当前该用户
        return userService.registerUser(userDao);

    }

    /**
     * 验证码生成  
     * 
     * @return
     */
    @GetMapping("/generateValidateCode")
    public Result generateValidateCode(){
        //验证码生成 
        return userService.generateValidateCode();
    }
    @GetMapping("/test1")
    public String test1(){
        //先校验数据库中是否有当前该用户
        return "Hello world!";
    }

    @RequestMapping("/loginIn")
    public Result loginIn(@RequestBody UserDao userDao ,@RequestHeader(value = "keep",required = false) String keep){
        //先校验数据库中是否有当前该用户
        return userService.loginUser(userDao);
    }

    /**
     *  两种方式获取token
     //* @param httpServletRequest
     * @return
     */
    @GetMapping(value = "/getUserInfoByToken")
    //public Result getUserInfo(@RequestHeader(name = "token") String token){
    public Result getUserInfo(HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader("token");
        Object o = JwtUtil.parseJwt(token).get("userTokenVO");
        UserTokenVO userTokenVO = JSON.parseObject(JSONObject.toJSONString(o), UserTokenVO.class);

        return Result.success(userTokenVO);
    }
    
    @GetMapping(value = "/getUserInfo")
    public Result<UserTokenVO> getUserInfo() {
        UserTokenVO userTokenVO = AuthContextUtil.getLoginUser();
        return Result.success(userTokenVO) ;
    }


    @PutMapping(value = "/deleteLoginUser")
    public Result deleteLoginUser() {
        // UserTokenVO userTokenVO = AuthContextUtil.getLoginUser();
        AuthContextUtil.deleteThreadLocal();
        return Result.success("退出成功") ;
    }

     //nacos 密钥生成方法
     // public static void main(String[] args) {
     //    SecureRandom secureRandom = new SecureRandom();
     //    byte[] key = new byte[32]; // 256位密钥
     //    secureRandom.nextBytes(key);
     //    // 将字节数组转换为Base64编码的字符串
     //    String secureKey = Base64.getEncoder().encodeToString(key);
     //    System.out.println(secureKey);
     // }
}
