package com.xs.heart.service.impl;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.xs.heart.entity.UserDao;
import com.xs.heart.entity.vo.UserLoginVO;
import com.xs.heart.entity.vo.UserTokenVO;
import com.xs.heart.utils.*;
import com.xs.heart.entity.User;

import com.xs.heart.mapper.UserMapper;
import com.xs.heart.service.UserService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.xs.heart.utils.RedisConstants.*;

/**
 * @author xs
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements UserService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;
    
    @Autowired
    private HttpServletResponse response;
    /**
     * 注册用户
     *
     * @param userDao
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result registerUser(UserDao userDao) {
        if (StringUtils.isEmpty(userDao.getUsername())){
            return Result.fail(ResultCodeEnum.REGISTER_USERINFO_ACCOUNT_NUll.getMessage());
        }

        User user = new User();
        BeanUtils.copyProperties(userDao,user);
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();

        wrapper.eq(User::getUsername,user.getUsername());

        User one = baseMapper.selectOne(wrapper);
        if (!StringUtils.isEmpty(one)){
               // 证明已经注册过了
            return Result.fail(ResultCodeEnum.REGISTER_USERINFO_ERROR.getMessage());
        }
        String uuid = UUID.randomUUID().toString().replace("-", "");
        user.setUserId(uuid);
        //
        String password = user.getPassword();
        //  加密密码 先放着吧 之后再做
        String encryptPassword = Md5Util.encryptPassword(password);
        //原来 这里有md5加密的方法
        //String encryptPassword= DigestUtils.md5DigestAsHex(password.getBytes());
        user.setEncryptPassword(encryptPassword);
        user.setCreateTime(DateUtils.getNowDate());
        user.setUpdateTime(DateUtils.getNowDate());
        //TODO 头像之后再做

        user.setAddr("北京（TODO以后再做）");
        if (null == user.getSignature()){
            user.setSignature("该heart用户比较懒，什么都没留下！！！");
        }

        baseMapper.insert(user);
        return Result.success(ResultCodeEnum.REGISTER_USERINFO_SUCCESS.getMessage());
    }

    /**
     * 生成验证码
     * @return 
     */
    @Override
    public Result generateValidateCode() {
        //通过工具类生成图片验证码
        CircleCaptcha circleCaptcha = CaptchaUtil.createCircleCaptcha(150, 48, 4, 20);
        //验证码的值
        String codeValue = circleCaptcha.getCode();
        // UserTokenVO loginUser = AuthContextUtil.getLoginUser();
        //将验证码的值存入redis并设置2分钟过期
//        stringRedisTemplate.opsForValue().set(LOGIN_CODE_KEY+"user",codeValue,LOGIN_CODE_TTL,TimeUnit.MINUTES);
        System.out.println("验证码为："+codeValue);
        //将图片进行base64编码，并返回
        String imageBase64 = circleCaptcha.getImageBase64();
        String key = UUID.randomUUID().toString().replaceAll("-", "");
   
        
        String imgUrl = "data:image/png;base64," + imageBase64;
        return Result.success(imgUrl);
    }

    /**
     * 登录用户
     *
     * @param userDao
     * @return
     */
    @Override
    public Result loginUser(UserDao userDao) {

        // 获取用户名
        String account = userDao.getUsername();
        if (StringUtils.isEmpty(account)){
            return Result.fail(ResultCodeEnum.REGISTER_USERINFO_ACCOUNT_NUll.getMessage());
        }
        User user = new User();
        BeanUtils.copyProperties(userDao,user);
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getUsername,account).eq(User::getDeleted,'1');

        User one = baseMapper.selectOne(wrapper);
        if (StringUtils.isEmpty(one)){
            return Result.fail(ResultCodeEnum.LOGIN_USERINFO_NOT_EXIST.getMessage());
        }
        
        UserTokenVO userTokenVO = new UserTokenVO();
        BeanUtils.copyProperties(one,userTokenVO);
        //登陆之后
        //AuthContextUtil.setThreadLocal(userTokenVO);
        String token = JwtUtil.generateJwt(userTokenVO);
        String encryptPassword = one.getEncryptPassword();
        // 对比加密密码
        String encryptPassword1 = Md5Util.encryptPassword(userDao.getPassword());

        UserLoginVO userLoginVO = new UserLoginVO();
        userLoginVO.setUserId(one.getUserId());
        userLoginVO.setToken(token);
        //userLoginVO.setBalance();
        userLoginVO.setUsername(one.getUsername());
        userLoginVO.setNickName(one.getNickName());
        if (encryptPassword.equals(encryptPassword1)){
            //因为stringredisTemplete是字符串，不转换会报bug
            Map<String, Object> userMap = BeanUtil.beanToMap(userLoginVO, new HashMap<>(),
                    CopyOptions.create()
                            .setIgnoreNullValue(true)
                            .setFieldValueEditor((fieldName, fieldValue) ->
                            {
                                if (fieldValue != null) {
                                    return fieldValue.toString();
                                } else {
                                    return null; // 或者您可以返回其他默认值
                                }
                            }));
            
            String tokenKey = LOGIN_USER_KEY + token;
            stringRedisTemplate.opsForHash().putAll(tokenKey,userMap);
            stringRedisTemplate.expire(tokenKey,LOGIN_USER_TTL, TimeUnit.MINUTES);
            return Result.success(userLoginVO);
        }else {
            return Result.fail(ResultCodeEnum.LOGIN_PASSWORD_ERROR.getMessage());
        }
        //
    }
}
