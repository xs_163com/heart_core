package com.xs.heart.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class UserLoginVO {
    private String token;
    private String userId;
    private String username;
    private String nickName;
    private String phoneNumber;
    private String gender;
    private String photoImgUrl;
    private String addr;
    private String signature;
}
