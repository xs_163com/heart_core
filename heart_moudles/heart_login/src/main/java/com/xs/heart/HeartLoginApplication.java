package com.xs.heart;


import com.xs.heart.entity.UserAuthProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Megtob
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties(value = {UserAuthProperties.class})
public class HeartLoginApplication {
    public static void main(String[] args) {
        SpringApplication.run(HeartLoginApplication.class, args);
    }
}
