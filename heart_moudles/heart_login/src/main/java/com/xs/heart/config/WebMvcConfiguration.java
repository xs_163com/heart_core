//package com.xs.heart.config;
//
//import com.xs.heart.entity.UserAuthProperties;
//import com.xs.heart.interceptor.LoginAuthInterceptor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//@Component
//public class WebMvcConfiguration implements WebMvcConfigurer {
//
//    @Autowired
//    private LoginAuthInterceptor loginAuthInterceptor ;
//
//    @Autowired
//    private UserAuthProperties userAuthProperties;
//
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(loginAuthInterceptor)
//                .excludePathPatterns(userAuthProperties.getNoAuthUrls())
//                //.addPathPatterns("/**");
//    }
//}
