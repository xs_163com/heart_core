package com.xs.heart.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xs.heart.entity.UserDao;
import com.xs.heart.utils.Result;
import com.xs.heart.entity.User;

/**
 * @author xs
 */
public interface UserService extends IService<User> {

    /**
     * 注册用户
     * @param userDao
     * @return
     */
    Result registerUser(UserDao userDao);

    /**
     * 登录用户
     * @param userDao
     * @return
     */
    Result loginUser(UserDao userDao);

    Result generateValidateCode();
    
    
}
