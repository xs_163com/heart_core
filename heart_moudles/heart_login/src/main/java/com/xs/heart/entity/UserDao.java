package com.xs.heart.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xs
 */
@Data
@AllArgsConstructor
public class UserDao implements Serializable {



    private String username;

    private String nickName;

    private String phoneNumber;

    private String gender;

    private String password;

    private String encryptPassword;

    private String photoImgUrl;

    private String signature;
    
    private String validateCode;
}
