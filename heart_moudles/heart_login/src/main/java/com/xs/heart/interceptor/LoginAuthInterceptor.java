package com.xs.heart.interceptor;

import com.alibaba.fastjson.JSON;
import com.xs.heart.entity.User;
import com.xs.heart.utils.AuthContextUtil;
import com.xs.heart.utils.Result;
import com.xs.heart.utils.ResultCodeEnum;
import com.xs.heart.utils.StringUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Megtob
 */
// @Component
public class LoginAuthInterceptor
       implements HandlerInterceptor {


    //@Autowired
    //private RedisTemplate redisTemplate;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 获取请求方式
        // 如果请求方式是options预检请求，直接放行
        String method = request.getMethod();
        if ("OPTIONS".equals(method)){
            return true;
        }


        //从请求头获取token
        String token = request.getHeader("token");
        if (StringUtils.isEmpty(token)){
            responseNoLoginInfo(response);
            return false;
        }


        // 如果token为空，则返回错误提示

        // 如果token不为空，那么此时验证token的合法性
        // TODO
        //String sysUserInfoJson = redisTemplate.opsForValue().get("user:login:" + token);
        //if(StrUtil.isEmpty(sysUserInfoJson)) {
        //    responseNoLoginInfo(response) ;
        //    return false ;
        //}
        //
        //如果不为空 拿着token查询redis


        // 如果redis查询到用户信息，把用户信息放到ThreadLocal中，
        //SysUser sysUser = JSON.parseObject(sysUserInfoJson, SysUser.class);
        User user = new User();
        //AuthContextUtil.setThreadLocal(user);

    // 重置Redis中的用户数据的有效时间
    //    redisTemplate.expire("user:login:" + token , 30 , TimeUnit.MINUTES) ;

        //把redis用户信息 的过期时间延长

        return true;
        //return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    //响应208状态码给前端
    private void responseNoLoginInfo(HttpServletResponse response) {
        Result<Object> result = Result.build(null, ResultCodeEnum.LOGIN_AUTH);
        PrintWriter writer = null;
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=utf-8");
        try {
            writer = response.getWriter();
            writer.print(JSON.toJSONString(result));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) writer.close();
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        // threadlocal中的数据移除
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
