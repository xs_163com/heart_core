package com.xs.heart.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xs.heart.entity.HeartMessage;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author xs
 */
@Data
public class MessageContentVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String messageId;
    private String photoImgUrl;
    private String nickName;

    /**
     * 消息
     */

    private String content;

    /**
     * 消息类型(1：文本 2：图片 3：表情)
     */

    private String messageType;

    /**
     * 消息id
     */
    //@TableField("message_id")
    //private String messageId;

    /**
     * 属于那个用户的消息
     */

    private String userId;

    /**
     * 接收消息用户id
     */

    private String receiverId;

    /**
     * 消息id
     // */
    //@TableField("msg_id")
    //private String msgId;

    /**
     * 1单聊  2群聊
     */

    private String talkType;

    /**
     * 是否撤回[1:否;2:是;]
     */

    private String isRevoke;

    /**
     * 消息扩展字段
     */

    private String extra;

    /**
     * 创建时间
     */

    private Date createTime;

    /**
     * 更新时间
     */

    private Date updateTime;

}
