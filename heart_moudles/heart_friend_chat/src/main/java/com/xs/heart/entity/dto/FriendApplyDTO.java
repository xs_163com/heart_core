package com.xs.heart.entity.dto;


import com.xs.heart.entity.BaseEntity;
import lombok.Data;

/**
 * @author Megtob
 */

@Data
public class FriendApplyDTO  extends BaseEntity {
    /**
     * 用户id
     */
    private String beApplyUserId;
    /**
     * 朋友id
     */
    private String agree;


}
