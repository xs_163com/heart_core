package com.xs.heart.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.xs.heart.entity.HeartUserFriendMessage;

import com.xs.heart.mapper.HeartUserFriendMessageMapper;


import com.xs.heart.service.HeartUserFriendMessageService;
import org.springframework.stereotype.Service;

/**
 * @author Megtob
 */
@Service
public class HeartUserFriendMessageServiceImpl extends ServiceImpl<HeartUserFriendMessageMapper, HeartUserFriendMessage> implements HeartUserFriendMessageService {


}
