package com.xs.heart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xs.heart.entity.HeartContactApply;

/**
 * <p>
 * 好友申请表 Mapper 接口
 * </p>
 *
 * @author
 * @since 2024-04-03
 */
public interface HeartContactApplyMapper extends BaseMapper<HeartContactApply> {

}
