package com.xs.heart.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.xs.heart.entity.HeartUserFriendMessage;


/**
 * @author Megtob
 */

public interface HeartUserFriendMessageService extends IService<HeartUserFriendMessage> {


}
