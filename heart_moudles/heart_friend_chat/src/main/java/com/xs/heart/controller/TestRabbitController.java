package com.xs.heart.controller;

import jakarta.annotation.Resource;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Megtob
 */
@RestController
@RequestMapping("/heart/testRabbit")
public class TestRabbitController {
    @Resource
    private RabbitTemplate rabbitTemplate;

    @GetMapping("/testRabbitmq")
    public void testRabbitmq(){
        // 队列名称
        String queueName = "heart.queue";
        // 消息
        String message = "hello, spring amqp!";

        rabbitTemplate.convertAndSend(queueName,message);
    }

    @GetMapping("/testWorkRabbitmq")
    public void testWorkRabbitmq() throws InterruptedException {
        // 队列名称
        String queueName = "work.queue";
        // 消息
        String  message = "hello, message_";

        for (int i = 0; i < 50; i++) {
            // 发送消息，每20毫秒发送一次，相当于每秒发送50条消息
            rabbitTemplate.convertAndSend(queueName, message + i);
            //Thread.sleep(2000);
        }
    }
    @GetMapping("/testFanoutExchange")
    public void testFanoutExchange() {
        // 交换机名称
        String exchangeName = "heart.fanout";
        // 消息
        String message = "hello, everyone!";
        rabbitTemplate.convertAndSend(exchangeName, "", message);
    }

    @GetMapping("/testSendDirectExchange")
    public void testSendDirectExchange() {
        // 交换机名称
        String exchangeName = "heart.direct";
        // 消息
        String redMessage = "红色警报！日本乱排核废水，导致海洋生物变异，惊现哥斯拉！";
        String blueMessage = "蓝色警报！日本枫可怜进入中国演艺圈。";
        String pinkMessage = "粉色警报！你中彩票了";
        // 发送消息
        rabbitTemplate.convertAndSend(exchangeName, "red", redMessage);
        rabbitTemplate.convertAndSend(exchangeName, "blue", blueMessage);
        rabbitTemplate.convertAndSend(exchangeName, "pink", pinkMessage);
    }
}
