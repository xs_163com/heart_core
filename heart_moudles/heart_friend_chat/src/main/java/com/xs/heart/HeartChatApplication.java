package com.xs.heart;

import com.xs.heart.netty.CoordinationNettyServer;
import jakarta.annotation.Resource;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

/**
 * @author xs
 */
@SpringBootApplication
@EnableDiscoveryClient
// @EnableConfigurationProperties(value = {UserAuthProperties.class})
public class HeartChatApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(HeartChatApplication.class, args);
    }


    /**
     *    这个是让rabbitmq放到队列的消息成为json格式的，要不然数据展现形式不友好
     * */
    @Bean
    public MessageConverter messageConverter(){
        // 1.定义消息转换器
        Jackson2JsonMessageConverter jackson2JsonMessageConverter = new Jackson2JsonMessageConverter();
        // 2.配置自动创建消息id，用于识别不同消息，也可以在业务中基于ID判断是否是重复消息
        jackson2JsonMessageConverter.setCreateMessageIds(true);
        return jackson2JsonMessageConverter;
    }

    @Resource
    private CoordinationNettyServer nettyServer;

    /**
     * Callback used to run the bean.
     *
     * @param args incoming main method arguments
     * @throws Exception on error
     */
    @Override
    public void run(String... args) throws Exception {
        nettyServer.start();
    }
}
