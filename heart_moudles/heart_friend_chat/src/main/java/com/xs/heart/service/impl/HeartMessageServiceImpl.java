package com.xs.heart.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xs.heart.entity.HeartMessage;
import com.xs.heart.mapper.HeartMessageMapper;
import com.xs.heart.service.HeartMessageService;
import org.springframework.stereotype.Service;

/**
 * @author Megtob
 */
@Service
public class HeartMessageServiceImpl extends ServiceImpl<HeartMessageMapper, HeartMessage> implements HeartMessageService {


}
