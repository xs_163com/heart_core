package com.xs.heart.entity.vo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.xs.heart.entity.BaseEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * <p>
 * 好友申请表
 * </p>
 *
 * @author
 * @since 2024-04-03
 */
@Data
@ToString
public class HeartContactApplyVO extends BaseEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 申请人ID
     */

    private String applyUserId;

    /**
     * 被申请人ID
     */

    private String beApplyUserId;

    /**
     * 申请备注
     */

    private String remark;

    /**
     * 申请时间（创建时间）
     */
    private Date createTime;
    /**
     *
     *     是否同意（1 已同意 2拒绝 3待处理）
     *
     */
    private String agree;

    private UserTokenVO userTokenVO;


}
