package com.xs.heart.service.impl;

import cn.hutool.core.stream.CollectorUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xs.heart.entity.User;

import com.xs.heart.mapper.UserMapper;

import com.xs.heart.service.UserService;
import com.xs.heart.utils.AuthContextUtil;
import com.xs.heart.utils.Result;
import com.xs.heart.utils.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @author xs
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements UserService {


    @Override
    public Result searchFriends(String name) {
        String userId = AuthContextUtil.getLoginUser().getUserId();
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(User::getUsername,name).or().like(User::getNickName,name);
        List<User> users = baseMapper.selectList(wrapper);
        if (StringUtils.isNotEmpty(users)){
            users = users.stream().filter(e -> !e.getUserId().equals(userId)).collect(Collectors.toList());
        }
        return Result.success(users);
    }


}
