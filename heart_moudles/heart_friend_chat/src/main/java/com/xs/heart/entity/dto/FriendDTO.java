package com.xs.heart.entity.dto;


import lombok.Data;

/**
 * @author Megtob
 */

@Data
public class FriendDTO {
    /**
     * 用户id
     */
    private String userId;
    /**
     * 朋友id
     */
    private String friendId;

    /**
     *   逻辑删除（1 未删除 2 已删除）
     */

    private String deleted;

}
