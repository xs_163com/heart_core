package com.xs.heart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xs.heart.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author xs
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}
