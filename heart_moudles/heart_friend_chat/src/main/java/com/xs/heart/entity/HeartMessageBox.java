package com.xs.heart.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * <p>
 * 好友消息盒子表
 * </p>
 *
 * @author
 * @since 2024-04-03
 */
@Getter
@Setter
@TableName("heart.heart_message_box")
public class HeartMessageBox extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private String userId;

    /**
     * 朋友id
     */
    @TableField("friend_id")
    private String friendId;

    /**
     * 消息id
     */
    @TableField("message_id")
    private String messageId;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 逻辑删除（1 未删除 2 已删除）
     */
    @TableField("deleted")
    // @TableLogic TODO 之后再配置默认的逻辑删除
    private String deleted;
}
