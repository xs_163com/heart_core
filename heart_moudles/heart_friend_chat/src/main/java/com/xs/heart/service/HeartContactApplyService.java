package com.xs.heart.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xs.heart.entity.HeartContactApply;

/**
 * <p>
 * 好友申请表 服务类
 * </p>
 *
 * @author
 * @since 2024-04-03
 */
public interface HeartContactApplyService extends IService<HeartContactApply> {

}
