package com.xs.heart.controller;

import com.xs.heart.entity.dto.FriendApplyDTO;
import com.xs.heart.entity.dto.FriendDTO;
import com.xs.heart.entity.dto.MessageContentDTO;
import com.xs.heart.entity.vo.UserTokenVO;
import com.xs.heart.service.FriendService;
import com.xs.heart.service.UserService;
import com.xs.heart.utils.AuthContextUtil;
import com.xs.heart.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author xs
 */
@RestController
@RequestMapping("/heart/chatAndFriend/chatFriend")
public class ChatFriendController {

    @Autowired
    private UserService userService;
    @Autowired
    private FriendService friendService;

    @GetMapping("/searchFriend/{name}")
    public Result searchFriend(@PathVariable String name){
    // public Result searchFriend(){
        // return Result.success("hello world");
        System.out.println(name);
        return userService.searchFriends(name);
    }

    @PostMapping("/toBeMyFriend")
    public Result pleaseToBeMyFriend(@RequestBody FriendDTO friendDTO){

        System.out.println(friendDTO);
        return friendService.pleaseToBeMyFriend(friendDTO);
    }

    @PostMapping("/agreeOrRefuseTOBeMyFriend")
    public Result agreeOrRefuseTOBeMyFriend(@RequestBody FriendApplyDTO friendApplyDTO){
        System.out.println(friendApplyDTO);
        return friendService.agreeOrRefuseTOBeMyFriend(friendApplyDTO);
    }

    @GetMapping("/getMyNewFriendList")
    public Result getMyNewFriendList(){
        // System.out.println(friendDTO);
        return friendService.getMyNewFriendList();
    }


    @GetMapping("/getBeMyNewFriendNo")
    public Result getBeMyNewFriendNo(){
        System.out.println();
        return friendService.getBeMyNewFriendNo();
    }

    @GetMapping("/getMyFriend")
    public Result getMyFriend(){
        //  到时候这里写一个单点登录直接从token取userId的方法，现在先写死哦 写完了
        UserTokenVO threadLocal = AuthContextUtil.getLoginUser();
        String userId = threadLocal.getUserId();
        System.out.println(userId);
        return friendService.getMyFriend(userId);
    }


    @GetMapping("/getMyMessageChatList")
    public Result getMyMessageChatList(){
        //  到时候这里写一个单点登录直接从token取userId的方法，现在先写死哦 写完了
        UserTokenVO threadLocal = AuthContextUtil.getLoginUser();
        String userId = threadLocal.getUserId();
        System.out.println(userId);
        return friendService.getMyMessageChatList(userId);
    }


    /**
     * 写一个新建聊天盒子的方法 这个方法可以没有消息内容
     * @return
     */
    @PostMapping("/newChatBox/{friendId}")
    public Result newChatBox(@PathVariable String friendId){
        return friendService.newChatBox(friendId);
    }


    /**
     * 获取聊天内容的方法
     * @return
     */
    @GetMapping("/getMessageContent/{messageId}/{friendId}")
    public Result getMessageContent(@PathVariable String messageId,@PathVariable String friendId){
        return friendService.getMessageContent(messageId,friendId);
    }


    /**
     * 获取聊天内容的方法
     * @return
     */
    @PostMapping ("/saveMessageContent")
    public Result saveMessageContent(@RequestBody MessageContentDTO messageContentDTO){
        return friendService.saveMessageContent(messageContentDTO);
    }

}
