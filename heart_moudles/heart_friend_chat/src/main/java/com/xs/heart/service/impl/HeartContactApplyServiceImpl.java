package com.xs.heart.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xs.heart.entity.HeartContactApply;
import com.xs.heart.mapper.HeartContactApplyMapper;
import com.xs.heart.service.HeartContactApplyService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 好友申请表 服务实现类
 * </p>
 *
 * @author
 * @since 2024-04-03
 */
@Service
public class HeartContactApplyServiceImpl extends ServiceImpl<HeartContactApplyMapper, HeartContactApply> implements HeartContactApplyService {

}
