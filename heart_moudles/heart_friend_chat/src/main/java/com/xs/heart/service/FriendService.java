package com.xs.heart.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.xs.heart.entity.HeartFriend;
import com.xs.heart.entity.dto.FriendApplyDTO;
import com.xs.heart.entity.dto.FriendDTO;
import com.xs.heart.entity.dto.MessageContentDTO;
import com.xs.heart.utils.Result;

/**
 * @author Megtob
 */
public interface FriendService extends IService<HeartFriend> {

    Result pleaseToBeMyFriend(FriendDTO friendDTO);

    Result getMyFriend(String userId);

    Result getMyMessageChatList(String userId);

    Result newChatBox(String friendId);


    Result getMessageContent(String messageId,String friendId);

    Result saveMessageContent(MessageContentDTO messageContentDTO);

    Result getBeMyNewFriendNo();


    Result getMyNewFriendList();

    Result agreeOrRefuseTOBeMyFriend(FriendApplyDTO friendApplyDTO);


}
