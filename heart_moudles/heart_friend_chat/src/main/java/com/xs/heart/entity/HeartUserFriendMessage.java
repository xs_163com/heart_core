package com.xs.heart.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author Megtob
 */
@TableName("heart_user_friend_message")
@Data
public class HeartUserFriendMessage {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    @TableField(value = "user_id")
    private String userId;
    @TableField(value = "friend_id")
    private String friendId;
    @TableField(value = "message_id")
    private String messageId;
    @TableField(value = "create_time")
    private Date createTime;

    /**
     *   逻辑删除（1 未删除 2 已删除）
     */
    @TableField(value = "deleted")
    private String deleted;
}
