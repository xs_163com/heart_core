package com.xs.heart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xs.heart.entity.HeartMessage;

import com.xs.heart.entity.HeartMessageBox;
import com.xs.heart.entity.HeartUserFriendMessage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Megtob
 */
@Repository
public interface HeartMessageMapper extends BaseMapper<HeartMessage> {

    HeartMessageBox selectByUserIdAndFriendId(@Param("friendId") String friendId, @Param("userId") String userId);



}
