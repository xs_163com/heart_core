package com.xs.heart.service.impl;
import java.util.Date;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xs.heart.entity.*;
import com.xs.heart.entity.dto.FriendApplyDTO;
import com.xs.heart.entity.dto.FriendDTO;
import com.xs.heart.entity.dto.MessageContentDTO;
import com.xs.heart.entity.vo.HeartContactApplyVO;
import com.xs.heart.entity.vo.MessageContentVO;
import com.xs.heart.entity.vo.MessageBoxVO;
import com.xs.heart.entity.vo.UserTokenVO;
import com.xs.heart.exception.CommonException;
import com.xs.heart.exception.XsExceptionHandler;
import com.xs.heart.mapper.*;
import com.xs.heart.service.FriendService;
import com.xs.heart.service.HeartMessageBoxService;
import com.xs.heart.utils.AuthContextUtil;
import com.xs.heart.utils.DateUtils;
import com.xs.heart.utils.Result;
import jakarta.annotation.Resource;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Megtob
 */
@Service
public class FriendServiceImpl extends ServiceImpl<FriendMapper, HeartFriend> implements FriendService {
    /**
     * @param userId
     * @return
     *
     */
    public final static String EXCHANGE_NAME = "heart.direct";
    public final static String QUEUE_NAME = "heart.addFriendQueue";
    public final static String ROUTING_KEY =  "addFriend";
    @Autowired
    private FriendMapper friendMapper;
    @Autowired
    private HeartMessageMapper heartMessageMapper;
    @Resource
    private HeartMessageBoxMapper heartMessageBoxMapper;

    @Resource
    private HeartMessageBoxService heartMessageBoxService;

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private HeartContactApplyMapper applyMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result agreeOrRefuseTOBeMyFriend(FriendApplyDTO friendApplyDTO) {
        String applyUserId = AuthContextUtil.getLoginUser().getUserId();
        String agree = friendApplyDTO.getAgree();
        String beApplyUserId = friendApplyDTO.getBeApplyUserId();
        LambdaQueryWrapper<HeartContactApply> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(HeartContactApply::getApplyUserId,beApplyUserId).eq(HeartContactApply::getBeApplyUserId,applyUserId);
        HeartContactApply heartContactApply = applyMapper.selectOne(wrapper);
        if (ObjectUtil.isEmpty(heartContactApply)){
            throw new CommonException("未知错误",1);
        }
        heartContactApply.setAgree(agree);
        heartContactApply.setUpdateTime(new Date());
        if (agree.equals("1")){
            heartContactApply.setAgreeTime(new Date());
        //    还差一步成为朋友
            HeartFriend heartFriend1 = new HeartFriend();
            heartFriend1.setUserId(beApplyUserId);
            heartFriend1.setFriendId(applyUserId);
            // heartFriend1.setRemark();
            // heartFriend1.setStatus();
            // heartFriend1.setGroupId();
            heartFriend1.setCreateTime(new Date());
            // heartFriend1.setUpdateTime();
            friendMapper.insert(heartFriend1);

            HeartFriend heartFriend2 = new HeartFriend();
            heartFriend2.setUserId(applyUserId);
            heartFriend2.setFriendId(beApplyUserId);
            heartFriend2.setCreateTime(new Date());
            friendMapper.insert(heartFriend2);


        }
        heartContactApply.setUpdateTime(new Date());
        applyMapper.updateById(heartContactApply);
        return Result.success("操作成功");
    }

    @Override
    public Result getMyNewFriendList() {

        String userId = AuthContextUtil.getLoginUser().getUserId();
        LambdaQueryWrapper<HeartContactApply> wrapper = new LambdaQueryWrapper<>();
        // 查询待处理的申请消息
        wrapper.eq(HeartContactApply::getBeApplyUserId,userId).eq(HeartContactApply::getAgree,"3");
        List<HeartContactApply> heartContactApplyList = applyMapper.selectList(wrapper);
        List<HeartContactApplyVO> applyListVO = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(heartContactApplyList)){
            for (HeartContactApply heartContactApply : heartContactApplyList) {
                UserTokenVO userTokenVO = new UserTokenVO();
                String applyUserId = heartContactApply.getApplyUserId();
                User user = friendMapper.selectUser(applyUserId);
                BeanUtils.copyProperties(user,userTokenVO);
                HeartContactApplyVO heartContactApplyVO = new HeartContactApplyVO();
                BeanUtils.copyProperties(heartContactApply,heartContactApplyVO);
                heartContactApplyVO.setUserTokenVO(userTokenVO);
                applyListVO.add(heartContactApplyVO);
            }
        }
        return Result.success(applyListVO);
    }

    @Override
    public Result getBeMyNewFriendNo() {
        // String userId = AuthContextUtil.getLoginUser().getUserId();
        String userId = AuthContextUtil.getLoginUser().getUserId();
        LambdaQueryWrapper<HeartContactApply> wrapper = new LambdaQueryWrapper<>();
        // 查询待处理的申请消息
        wrapper.eq(HeartContactApply::getBeApplyUserId,userId).eq(HeartContactApply::getAgree,"3");
        List<HeartContactApply> heartContactApplyList = applyMapper.selectList(wrapper);
        int size = 0;
        if (CollectionUtil.isNotEmpty(heartContactApplyList)){
            size = heartContactApplyList.size();
        }
        return Result.success(size);
    }

    @Override
    public Result saveMessageContent(MessageContentDTO messageContentDTO) {
        // TODO 这个估计后面要用netty实现

        UserTokenVO threadLocal = AuthContextUtil.getLoginUser();

        HeartMessage heartMessage = new HeartMessage();

        //heartMessage.setId();
        heartMessage.setContent(messageContentDTO.getTextarea());
        heartMessage.setMessageType(messageContentDTO.getMessageType());
        heartMessage.setUserId(threadLocal.getUserId());
        heartMessage.setReceiverId(messageContentDTO.getFriendId());
        heartMessage.setTalkType(messageContentDTO.getTalkType());

        heartMessage.setCreateTime(DateUtils.getNowDate());
        heartMessage.setMsgBoxId(messageContentDTO.getMessageId());

        heartMessage.setContent(messageContentDTO.getTextarea());
        heartMessage.setMessageType(messageContentDTO.getMessageType());
        heartMessage.setCreateTime(DateUtils.getNowDate());

        heartMessage.setUserId(threadLocal.getUserId());
        heartMessage.setTalkType(messageContentDTO.getTalkType());

        heartMessageMapper.insert(heartMessage);
        return Result.success("消息发送成功");
    }

    @Override
    public Result getMessageContent(String messageId,String friendId) {
        LambdaQueryWrapper<HeartMessage> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(HeartMessage::getDeleted,"1").eq(HeartMessage::getMsgBoxId,messageId);
        List<HeartMessage> heartMessageList = heartMessageMapper.selectList(wrapper);
        //
        // LambdaQueryWrapper<HeartUserFriendMessage> wrapper1 = new LambdaQueryWrapper<>();
        // wrapper1.eq(HeartUserFriendMessage::getDeleted,"1").eq(HeartUserFriendMessage::getMessageId,messageId);
        // HeartUserFriendMessage heartUserFriendMessage = heartUserFriendMessageMapper.selectOne(wrapper1);
        // MessageContentVO messageVO = new MessageContentVO();
        // if (ObjectUtil.isNotEmpty(heartUserFriendMessage)){
        //
        // }
        String userId = AuthContextUtil.getLoginUser().getUserId();
        User loginUser = friendMapper.selectUser(userId);
        User friend = friendMapper.selectUser(friendId);

        List<MessageContentVO> messageVOList = new ArrayList<>();
        for (HeartMessage heartMessage : heartMessageList) {
            MessageContentVO messageVO = new MessageContentVO();
            messageVO.setId(heartMessage.getId());
            messageVO.setUserId(heartMessage.getUserId());
            messageVO.setContent(heartMessage.getContent());
            messageVO.setMessageId(messageId);
            messageVO.setCreateTime(heartMessage.getCreateTime());
            messageVO.setReceiverId(heartMessage.getReceiverId());
            messageVO.setMessageType(heartMessage.getMessageType());
            messageVO.setTalkType(heartMessage.getTalkType());
            messageVO.setIsRevoke(heartMessage.getIsRevoke());
            messageVO.setExtra(heartMessage.getExtra());
            if (heartMessage.getUserId().equals(loginUser.getUserId())){
                messageVO.setPhotoImgUrl(loginUser.getPhotoImgUrl());
                messageVO.setNickName(loginUser.getNickName());
            }else {
                messageVO.setPhotoImgUrl(friend.getPhotoImgUrl());
                messageVO.setNickName(friend.getNickName());
            }
            messageVOList.add(messageVO);
        }


        return Result.success(messageVOList);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result newChatBox(String friendId) {

        UserTokenVO threadLocal = AuthContextUtil.getLoginUser();
        String userId = threadLocal.getUserId();
        //应该先去查询是否有对应的消息盒子

        HeartMessageBox heartMessageBox = heartMessageMapper.selectByUserIdAndFriendId(friendId, userId);
        HeartMessageBox heartMessageBox1 = heartMessageMapper.selectByUserIdAndFriendId(userId, friendId);
        String resultMessageId = null;

        if (ObjectUtil.isEmpty(heartMessageBox) ){
            HeartMessageBox heartMessageBox2 = heartMessageMapper.selectByUserIdAndFriendId(userId, friendId);
            if (ObjectUtil.isEmpty(heartMessageBox2) ){
                //    如果为null 重新创建一个消息盒子
                String messageId = UUID.randomUUID().toString().replaceAll("-","");
                resultMessageId = messageId;
                HeartMessageBox messageBox = new HeartMessageBox();
                messageBox.setUserId(userId);
                messageBox.setFriendId(friendId);
                messageBox.setMessageId(messageId);
                messageBox.setCreateTime(DateUtils.getNowDate());
                heartMessageBoxMapper.insert(messageBox);

                HeartMessageBox messageBox1 = new HeartMessageBox();
                messageBox1.setUserId(friendId);
                messageBox1.setFriendId(userId);
                messageBox1.setMessageId(messageId);
                messageBox1.setCreateTime(DateUtils.getNowDate());

                heartMessageBoxMapper.insert(messageBox1);
            }else {
                String deleted = heartMessageBox2.getDeleted();
                if ("2".equals(deleted)){
                    deleted = "1";
                }
                heartMessageBox2.setDeleted(deleted);
                heartMessageBoxService.saveOrUpdate(heartMessageBox2);

                String messageId = heartMessageBox2.getMessageId();
                resultMessageId = messageId;
                HeartMessageBox messageBox = new HeartMessageBox();
                messageBox.setUserId(userId);
                messageBox.setFriendId(friendId);
                messageBox.setMessageId(messageId);
                messageBox.setCreateTime(DateUtils.getNowDate());
                heartMessageBoxMapper.insert(messageBox);
            }

        }else {
            String deleted = heartMessageBox.getDeleted();
            if ("2".equals(deleted)){
                deleted = "1";
            }
            heartMessageBox.setDeleted(deleted);
            heartMessageBoxService.saveOrUpdate(heartMessageBox);
            // 不为空的话
            resultMessageId = heartMessageBox.getMessageId();
        }
        return Result.success(resultMessageId);
    }

    /**
     * @param userId
     * @return
     */
    @Override
    public Result getMyMessageChatList(String userId) {

        List<MessageBoxVO> messageBoxVOList = new ArrayList<>();


        // 从新写这个接口
        // 首先获取登录人的userid  然后去heart_message_box表中查询 有没有这个人的消息盒子 应该是两个人共用一个消息盒子
        // 1 查询自己是用户的
        LambdaQueryWrapper<HeartMessageBox> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(HeartMessageBox::getUserId,userId).eq(HeartMessageBox::getDeleted,"1");
        List<HeartMessageBox> heartMessageBoxList = heartMessageBoxMapper.selectList(wrapper);
        if (!CollectionUtil.isEmpty(heartMessageBoxList)){
        //    不为空
            for (HeartMessageBox messageBox : heartMessageBoxList) {
                MessageBoxVO messageBoxVO = new MessageBoxVO();
                String messageId = messageBox.getMessageId();
                LambdaQueryWrapper<HeartMessage> wrapper1 = new LambdaQueryWrapper<>();
                wrapper1.eq(HeartMessage::getMsgBoxId,messageId).eq(HeartMessage::getDeleted,"1");
                List<HeartMessage> heartMessageList = heartMessageMapper.selectList(wrapper1);
                if (CollectionUtil.isNotEmpty(heartMessageList)){
                    //不空 那么进行排序 取出最新一条的消息
                    List<HeartMessage> collect = heartMessageList.stream().sorted(Comparator.comparing(HeartMessage::getCreateTime).reversed()).collect(Collectors.toList());
                    // todo 这里应该还要区分图片或者文件啥的，之后再优化吧
                    String content = collect.get(0).getContent();
                    messageBoxVO.setLatestNews(content);
                    messageBoxVO.setMessageCreateTime(collect.get(0).getCreateTime());

                    //for (HeartMessage heartMessage : heartMessageList) {
                    //    heartMessage.get
                    //}
                }
                String friendId = messageBox.getFriendId();
                User  user = friendMapper.selectUser(friendId);
                UserTokenVO userTokenVO = new UserTokenVO();
                BeanUtils.copyProperties(user,userTokenVO);
                messageBoxVO.setUserTokenVO(userTokenVO);
                messageBoxVO.setMessageId(messageBox.getMessageId());
                if (null == messageBoxVO.getMessageCreateTime()){
                    messageBoxVO.setMessageCreateTime(messageBox.getCreateTime());
                }
                messageBoxVO.setUserId(messageBox.getUserId());
                messageBoxVO.setFriendId(messageBox.getFriendId());

                messageBoxVOList.add(messageBoxVO);

                //messageBoxVO.setMessageList(heartMessageList);
            }
        }

        return Result.success(messageBoxVOList);
    }

    @Override
    public Result getMyFriend(String userId) {

        List<User> myfriends = friendMapper.getMyfriends(userId);
        // LambdaQueryWrapper<HeartFriend> wrapper = new LambdaQueryWrapper<>();
        return Result.success(myfriends);
    }

    /**
     * @param friendDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result pleaseToBeMyFriend(FriendDTO friendDTO) {

        // Friend friend = new Friend();
        // BeanUtils.copyProperties(friendDTO,friend);
        UserTokenVO threadLocal = AuthContextUtil.getLoginUser();
        HeartContactApply heartContactApply = new HeartContactApply();
        User user = friendMapper.selectUser(threadLocal.getUserId());
        LambdaQueryWrapper<HeartContactApply> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(HeartContactApply::getApplyUserId,friendDTO.getUserId()).eq(HeartContactApply::getBeApplyUserId,friendDTO.getFriendId());

        HeartContactApply heartContactApply1 = applyMapper.selectOne(wrapper);
        if (ObjectUtil.isNotEmpty(heartContactApply1)){
            String agree = heartContactApply1.getAgree();
            if ("2".equals(agree)){
            //    如果是“2,更新这条消息吧
                heartContactApply1.setAgree("3");
                applyMapper.updateById(heartContactApply1);
                return Result.success("添加成功,请等待对方同意");
            }else if ("3".equals(agree)){
                return Result.success("您已发送添加朋友通知,请等待对方同意");
            }else{
                return Result.success("您和对方已是朋友");
            }
        }else {
            heartContactApply.setApplyUserId(friendDTO.getUserId());
            heartContactApply.setBeApplyUserId(friendDTO.getFriendId());
            heartContactApply.setRemark("您好，我是"+user.getNickName());
            heartContactApply.setCreateTime(new Date());
            // heartContactApply.setUpdateTime(new Date());
            // heartContactApply.setAgreeTime(new Date());
            applyMapper.insert(heartContactApply);

            // 发送到rabbitmq的添加朋友队列里面
            String exchangeName = "heart.direct";
            String queueName = "heart.addFriendQueue";
            String routingKey =  "addFriend";
            HeartContactApplyVO applyVo = new HeartContactApplyVO();
            applyVo.setBeApplyUserId(friendDTO.getFriendId());
            applyVo.setApplyUserId(friendDTO.getUserId());
            // applyVo.setRemark();
            applyVo.setCreateTime(new Date());
            // rabbitTemplate.convertAndSend(EXCHANGE_NAME,ROUTING_KEY,applyVo);
            return Result.success("添加成功,请等待对方同意");
        }

    }
}
