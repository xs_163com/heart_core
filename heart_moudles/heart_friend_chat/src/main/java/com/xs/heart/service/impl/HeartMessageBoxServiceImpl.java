package com.xs.heart.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xs.heart.entity.HeartMessageBox;
import com.xs.heart.mapper.HeartMessageBoxMapper;
import com.xs.heart.service.HeartMessageBoxService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 好友消息盒子表 服务实现类
 * </p>
 *
 * @author
 * @since 2024-04-03
 */
@Service
public class HeartMessageBoxServiceImpl extends ServiceImpl<HeartMessageBoxMapper, HeartMessageBox> implements HeartMessageBoxService {

}
