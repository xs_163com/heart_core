package com.xs.heart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xs.heart.entity.HeartFriend;
import com.xs.heart.entity.User;
import com.xs.heart.entity.vo.HeartMessageVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Megtob
 */
@Repository
public interface FriendMapper extends BaseMapper<HeartFriend> {

    /**
     * @param useId
     * @return
     */
    List<User> getMyfriends(@Param("useId") String useId);

    List<HeartMessageVO> getMessages(String userId);

    User selectUser(@Param("friendId") String friendId);

}
