package com.xs.heart.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Date;

/**
 * @author Megtob
 */
@Data
public class HeartUserFriendMessageVO {



    private Long userId;

    private String friendId;

    private Long messageId;

    private Date createTime;
    /**
     *   逻辑删除（1 未删除 2 已删除）
     */

    private String deleted;
}
