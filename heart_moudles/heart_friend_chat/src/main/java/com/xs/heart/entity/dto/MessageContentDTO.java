package com.xs.heart.entity.dto;


import lombok.Data;

import java.io.Serializable;

/**
 * @author xs
 */
@Data
public class MessageContentDTO implements Serializable {
    private String  textarea;
    private String messageId;
    private String talkType;
    private String messageType;
    private String friendId;
}
