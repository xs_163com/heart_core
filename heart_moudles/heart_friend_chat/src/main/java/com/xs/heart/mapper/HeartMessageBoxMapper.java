package com.xs.heart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xs.heart.entity.HeartMessageBox;

/**
 * <p>
 * 好友消息盒子表 Mapper 接口
 * </p>
 *
 * @author
 * @since 2024-04-03
 */
public interface HeartMessageBoxMapper extends BaseMapper<HeartMessageBox> {

}
