package com.xs.heart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xs.heart.entity.HeartMessage;
import com.xs.heart.entity.HeartUserFriendMessage;
import org.springframework.stereotype.Repository;

/**
 * @author Megtob
 */
@Repository
public interface HeartUserFriendMessageMapper extends BaseMapper<HeartUserFriendMessage> {

}
