package com.xs.heart.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xs.heart.entity.HeartMessage;

/**
 * @author Megtob
 */
public interface HeartMessageService extends IService<HeartMessage> {


}
