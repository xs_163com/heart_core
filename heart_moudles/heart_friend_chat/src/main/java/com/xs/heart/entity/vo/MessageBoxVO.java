package com.xs.heart.entity.vo;

import com.xs.heart.entity.HeartMessage;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author Megtob
 */
@Data
public class MessageBoxVO {

    private String userId;

    private String friendId;
    private String messageId;
    private UserTokenVO userTokenVO;
    private List<HeartMessage> messageList;
    // 最新的额一条消息，展示用 可以为null
    private String latestNews;
    private Date messageCreateTime;
    private Date messageBoxCreateTime;

    /**
     *   逻辑删除（1 未删除 2 已删除）
     */

    private String deleted;

}
