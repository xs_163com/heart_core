package com.xs.heart.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xs.heart.entity.HeartMessageBox;

/**
 * <p>
 * 好友消息盒子表 服务类
 * </p>
 *
 * @author
 * @since 2024-04-03
 */
public interface HeartMessageBoxService extends IService<HeartMessageBox> {

}
