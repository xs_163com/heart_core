package com.xs.heart.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xs.heart.entity.User;
import com.xs.heart.utils.Result;

/**
 * @author xs
 */
public interface UserService extends IService<User> {
    Result searchFriends(String name);

}
