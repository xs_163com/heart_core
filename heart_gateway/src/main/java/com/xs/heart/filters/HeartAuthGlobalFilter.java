package com.xs.heart.filters;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.xs.heart.config.UserAuthProperties;
import com.xs.heart.entity.vo.UserTokenVO;
import com.xs.heart.utils.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author xs
 */
@Component
@RequiredArgsConstructor
public class HeartAuthGlobalFilter implements GlobalFilter, Ordered {
    public static final Integer ORDER = 0;

    private  final UserAuthProperties authProperties;
    private final AntPathMatcher antPathMatcher = new AntPathMatcher();  // 通配符路径匹配器
    /**
     * Process the Web request and (optionally) delegate to the next {@code GatewayFilter}
     * through the given {@link GatewayFilterChain}.
     *
     * @param exchange the current server exchange
     * @param chain    provides a way to delegate to the next filter
     * @return {@code Mono<Void>} to indicate when request processing is complete
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        //1 获取请求头
        ServerHttpRequest request = exchange.getRequest();
        //2 判断是否需要做登录校验
        List<String> noAuthUrls = authProperties.getNoAuthUrls();
        if (isNoAuthUrls(noAuthUrls,request.getPath().toString())){
            return chain.filter(exchange);
        }
        //3 获取token
        // List<String> headerList = request.getHeaders().get("authorization");
        List<String> headerList = request.getHeaders().get("token");
        String token = null;
        if (null != headerList && !headerList.isEmpty()){
            token = headerList.get(0);
        }

        //4 校验并解析token
        // 没有，先假装有吧
        String userId = null;
        UserTokenVO userTokenVO = null;
        try {
            Object o = JwtUtil.parseJwt(token).get("userTokenVO");
            userTokenVO = JSONObject.parseObject(JSON.toJSONString(o), UserTokenVO.class);
        }catch (Exception e){
            e.printStackTrace();
            ServerHttpResponse response = exchange.getResponse();
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
        }
        UserTokenVO sendUserTokenVO = userTokenVO;
        String sendUserJsonStr = JSON.toJSONString(sendUserTokenVO);
        //5 传递用户信息
        ServerWebExchange swe = exchange.mutate().request(builder -> builder.header("userInfo", sendUserJsonStr))
                .build();

        //6 这一步是放行，传给下一个过滤器，不写的话没法让下一个过滤器做校验
        return chain.filter(swe);
    }

    private boolean isNoAuthUrls(List<String> noAuthUrls, String requestPath) {

        for (String noAuthUrl : noAuthUrls) {
            if (antPathMatcher.match(noAuthUrl,requestPath)){
                return true;
            }

        }
        return false;
    }

    /**
     * Get the order value of this object.
     * <p>Higher values are interpreted as lower priority. As a consequence,
     * the object with the lowest value has the highest priority (somewhat
     * analogous to Servlet {@code load-on-startup} values).
     * <p>Same order values will result in arbitrary sort positions for the
     * affected objects.
     *
     * @return the order value
     * @see #HIGHEST_PRECEDENCE
     * @see #LOWEST_PRECEDENCE
     */
    @Override
    public int getOrder() {
        return ORDER;
    }
}
