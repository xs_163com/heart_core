package com.xs.heart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author xs
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class HeartGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(HeartGatewayApplication.class, args);
	}

}

